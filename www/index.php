<?php
  include "corefunctions.php";
?>

<!-- include the html header -->
<?php pageHeader( "main" ); ?>

<body id="altbody">

<!-- create the menu, index is selected -->
<?php pageMenu( "main" ); ?>

<div id="content">
  <h2>news.</h2>
  <ul>
    <li><strong>20100122</strong>: <a href="download.php">cerberus 0.2.1</a> available</li>
    <li><strong>20090730</strong>: <a href="download.php">cerberus 0.2</a> available</li>
    <li><strong>20090723</strong>: improved webpage, <a href="download.php">source</a> available</li>
    <li><strong>20090614</strong>: created webpage, no release yet</li>
  </ul>

  <h2>overview.</h2>
  <div style="display:block;float:right;margin: 5px 5px 5px 10px;">   
		<script type="text/javascript"><!--
			google_ad_client = "pub-6263538305442568";
			/* cerberus, 120x240, overview */
			google_ad_slot = "2237380291";
			google_ad_width = 120;
			google_ad_height = 240;
			//-->
		</script>
		<script type="text/javascript"
			src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
		</script>
	</div>
  <p>Cerberus is a remake of the old game classic "The Sentinel" which
     was created by mastermind
     <a href="http://en.wikipedia.org/wiki/Geoff_Crammond">Geoff Crammond</a>
     in 1986. In this game
     a Sentinel, which rules a landscape by distributing the energy
     represented by trees, boulders and robots, evenly across the
     landscape, must be absorbed. After the absorption of the Sentinel
     one becomes himself the ruler of the landscape and is able to
     leave and hyperspace to the next world.</p>
  <p>I wrote this game mainly to learn a game engine like
     <a href="http://irrlicht.sourceforge.net/">Irrlicht</a> (and
     because it was most of the time fun). There are already some
     other "The Sentinel" remakes out there (see links section), most
     of them free, but none of them open-source and cross platform.</p>
  <p>I also tried to code the game very similar to the original. After
     a first working version I may extend the gameplay and
     graphics. But there will be always a classic mode - back when the
     game was published I played it a lot - it was a great game.</p>

  <h2>screenshots.</h2>
  <p>Here are some screenshots for your viewing pleasure:</p>
  <p>
	<a href="screenshots/screenshot2.png" rel="lightbox[cerberus]" title="Menu">
		<img src="screenshots/screenshot2.png" width="270" /></a>
	<a href="screenshots/screenshot3.png" rel="lightbox[cerberus]" title="Start of games">
		<img src="screenshots/screenshot3.png" width="270" /></a>
	<a href="screenshots/screenshot4.png" rel="lightbox[cerberus]" title="Mission failed :(">
		<img src="screenshots/screenshot4.png" width="270" /></a>
  </p>
  <p><a href="screenshots.php">More screenshots ...</a>
  </p>

  <h2>links.</h2>
  <div style="display:block;float:right;margin: 5px 5px 5px 10px;">   
		<script type="text/javascript"><!--
			google_ad_client = "pub-6263538305442568";
			/* cerberus, 120x240, overview */
			google_ad_slot = "2237380291";
			google_ad_width = 120;
			google_ad_height = 240;
			//-->
		</script>
		<script type="text/javascript"
			src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
		</script>
	</div>
  <p>A lot information about the Game "The Sentinel" from the famous mastermind Geoff Crammond can be 
  	 found at Wikipedia <a href="http://en.wikipedia.org/wiki/The_Sentinel_(computer_game)">
  	 http://en.wikipedia.org/wiki/The_Sentinel_(computer_game)</a></p>
  <p>There are already some other Sentinel clones and remakes out there, but none is Open Source
     or cross platform.
		<ul>
			<li><a href="http://eicart.free.fr/sentry/">Sentry</a>: Freeware, Windows 95/98, abandoned 2000</li>
			<li><a href="http://www.georg-rottensteiner.de/en/index.htmll">Sentinel</a>: Freeware, Windows 95/98/2000/XP</li>
			<li><a href="http://www.addink.net/sentinel/">Sentinel Redux</a>: Freeware, Flash, abandoned 2005?</li>
			<li><a href="http://johnvalentine.co.uk/zenith.php?art=index">Zenith</a>: Freeware, Windows</li>
  	</ul>
  </p>
  <p>To develop this game some great tools were used:
    <ul>
      <li><a href="http://irrlicht.sourceforge.net/">Irrlicht</a>: cross platform realtime 3D engine</li>
      <li><a href="http://www.ambiera.com/irrklang/">IrrKlang</a>: cross platform 3D sound engine</li>
      <li><a href="http://www.cmake.org">CMake</a>: cross platform build system</li>
      <li><a href="http://www.wings3d.org">Wings3d</a>: 3D modelling software</li>
    </ul>
  </p>

<h2>miscellaneous.debris.</h2>
  <p>The design of this webpage was taken from <a href="http://www.oswd.org">http://www.oswd.org</a>
     - it's called <a href="http://www.oswd.org/design/preview/id/2845">neuphoric</a>. The nice popup effect
    for the screenshots is done with the help of
    <a href="http://www.digitalia.be/software/slimbox">Slimbox v1.41</a>.
  </p>
  <p>I used several free sounds, music and art for cerberus:
    <ul>
      <li>None</li>
    </ul>
  </p>
</div>

<!-- include the page footer -->
<?php pageFooter(); ?>

</body>
</html>
