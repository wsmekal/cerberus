<?php
  include "corefunctions.php";
?>

<!-- include the html header -->
<?php pageHeader( "manual" ); ?>

<body id="altbody">

<!-- create the menu, index is selected -->
<?php pageMenu( "manual" ); ?>

<div id="content">
	<strong>cerberus</strong><br />
  <strong>Created by Werner Smekal, July 2009</strong><br />
  <strong>v0.1</strong><br />

  <h2>overview.</h2>
  <p>Cerberus is a remake of the old game classic "The Sentinel" which
     was created by mastermind Geoff Crammond in 1986. In this game
     a Sentinel, which rules a landscape by distributing the energy
     represented by trees, boulders and robots, evenly across the
     landscape, must be absorbed. After the absorption of the Sentinel
     one becomes himself the ruler of the landscape and is able to
     leave and hyperspace to the next world where the next ruler
     awaits you.</p>
	
  <h2>game.play.</h2>
  <p>After the game is started the title screen will be presented. Press a
  	 key to proceed to the landscape selection screen. Enter a number between 0000
     and 9999 to choose the landscape you want to rule. A 8-digit secret entry code
     is needed to access the landscape, except landscape 0000 where no secret code
     is needed. If the code was correct an aerial view of the landscape will be
     shown, where you can spot the positions of the Cerberus and his helpers, if any.
     After a key press you'll be taken to the landscape's surface. There is no
     action taken by the Cerberus nor his helpers until you absorb energy or
     create something, giving you time to look around and considering your situation.
  </p>

  <h3>available.objects.</h3>
  <ul>
  <li>tree - 1 unit (will not be absorbed by Cerberus/helpers</li>
  <li>boulder - 2 units</li>
  <li>robot - 3 units (Yip, that is you!)</li>
  <li></li>
  
  </ul>

  <h3>absorb.objects.</h3>
	<p>
	</p>

  <h3>create.objects.</h3>
	<p>
	</p>

	<h2>controls.</h2>
	<p>
	<ul>
		<li>F4 ... Pause</li>
		<li>F3 ... Continue</li>
		<li>Space ... Cursor on/o	ff</li>
		<li>T ... Create Tree</li>
		<li>B ... Create Boulder</li>
		<li>R ... Create Robot</li>
		<li>Q ... Transfer to another Robot</li>
		<li>A ... Absorb</li>
		<li>H ... Hyperspace</li>
	</ul>
	To quit a game and return to the title page, press the ESC key.
	To quit the application, press ESC from the title page.
	</p>	

  <h2>basics.</h2>
	<p>basics
  </p>
	
  <h2>entities.</h2>
	<p>entities
  </p>

	<h3>robot</h3>
	<strong>Energy</strong>: 3<br />
	<p>robot
	</p>

	<h2>world</h2>
	<p>world
	</p>

	<h2>tips.</h2>
	<ul>
  	<li>Each time a cerberus or meanie rotates, there will be a low-pitched bleep.</li>
  	<li>Birds-eye views can be obtained by looking up into the sky and pressing
  	    the function key 'F1'</li>
	</ul>

	<h2>thanks.to.</h2>
	<p>who?</p>

	<h2>acknowledgements.</h2>
	<p>some</p>

	<p>Questions? Comments? Suggestions? Bug Reports? Contact the creator of this game
		 <a href="mailto:cerberus@miscdebris.net">cerberus@miscdebris.net</a> 
	</p>
</div>

<!-- include the page footer -->
<?php pageFooter(); ?>

</body>
</html>
