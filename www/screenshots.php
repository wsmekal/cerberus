<?php
  include "corefunctions.php";
?>

<!-- include the html header -->
<?php pageHeader( "screenshots" ); ?>

<body id="altbody">

<!-- create the menu, index is selected -->
<?php pageMenu( "screenshots" ); ?>

<div id="content">
	<h2>Screenshots from the game</h2>
	<div style="display:block;float:center;margin: 5px 5px 5px 5px;">   
	  <script type="text/javascript"><!--
			google_ad_client = "pub-6263538305442568";
			/* cerberus, 468x60, banner */
			google_ad_slot = "9196108612";
			google_ad_width = 468;
			google_ad_height = 60;
			//-->
		</script>	
		<script type="text/javascript"
			src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
		</script>
	</div>
	<p>
	<a href="screenshots/screenshot1.png" rel="lightbox[zct]" title="Menu">
		<img src="screenshots/screenshot1.png" width="213" height="160" /></a>
	<a href="screenshots/screenshot2.png" rel="lightbox[zct]" title="Menu">
		<img src="screenshots/screenshot2.png" width="213" height="160" /></a>
	<a href="screenshots/screenshot3.png" rel="lightbox[zct]" title="Start of a game">
		<img src="screenshots/screenshot3.png" width="213" height="160" /></a>
	</p>
	<p>
	<a href="screenshots/screenshot4.png" rel="lightbox[zct]" title="Mission failed">
		<img src="screenshots/screenshot4.png" width="213" height="160" /></a>
	</p>
</div>

<!-- include the page footer -->
<?php pageFooter(); ?>

</body>
</html>
