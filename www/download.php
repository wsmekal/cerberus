<?php
  include "corefunctions.php";
?>

<!-- include the html header -->
<?php pageHeader( "download" ); ?>

<body id="altbody">

<!-- create the menu, index is selected -->
<?php pageMenu( "download" ); ?>

<div id="content">
	<h2>binaries.</h2>
	<h3>Mac OS X</h3>
	<p>Download the dmg file and open it. Copy the cerberus app to /Applications or any other 
	   location you like. The app is completely self contained as usual.</p>
	<ul>
		<li><a href="downloads/cerberus-0.2.1-Darwin.dmg">cerberus-0.2.1-Darwin.dmg</a> (14.3MB)</li>
	</ul>

	<h2>source.code.</h2>
	<p>In the moment I only provide the source code to cerberus. The 
	   <a href="docs/html/index.html">documentation</a> of the source code is done
	   with the help of <a href="http://www.doxygen.org">Doxygen</a>. Since the
	   game is not really in a playable state, it doesn't make sense to provide
	   binaries:
	   <ul>
	   		<li><a href="downloads/cerberus-source-0.2.1.tar.gz">cerberus-source-0.2.1.tar.gz</a> (20100122, approx. 50Mb)</li>
	   		<li><a href="downloads/cerberus-source-0.2.tar.gz">cerberus-source-0.2.tar.gz</a> (20090730, approx. 10Mb)</li>
	   		<li><a href="downloads/cerberus-source-0.1.tar.gz">cerberus-source-0.1.tar.gz</a> (20090723, approx. 10Mb)</li>
	   </ul>
	</p>
	<p>
	   The instructions to compile the source code are in the package and below.
	   It's also possible to get the bleeding edge source code by checking it out
		 from the svn repository:<br />
		 <tt>svn checkout http://svn.miscdebris.net/cerberus/</tt>
	</p>

	<h2>compile.instructions</h2>
	<h3>Mac OS X</h3>
	<ul>
		<li>XCode must be installed obviously</li>
		<li>Download <a href=�http://irrlicht.sourceforge.net/downloads.html�>Irrlicht 1.5 SDK</a> (irrlich-1.5.zip) and unzip it</li>
		<li>cd into <tt>irrlicht-1.5/source/Irrlicht/MacOSX</tt></li>
		<li>run: <tt>xcodebuild -project MacOSX.xcodeproj -configuration Debug</tt> or <tt>xcodebuild -project MacOSX.xcodeproj -configuration Release</tt></li>
		<li>run: <tt>cp build/Debug/libIrrlicht.a ../../../lib/MacOSX/</tt></li>
		<li>Download the <a href=�http://www.ambiera.com/irrklang/downloads.html�>irrKlang 1.1.3c</a> library and unzip it</li>
		<li>Set <tt>CMAKE_LIBRARY_PATH</tt> and <tt>CMAKE_INCLUDE_PATH</tt> to the corresponding irrKlang and Irrlicht include and library directory
		    in order for Cmake to find everything, e.g. add to the <tt>.profile</tt> file in your home directory

				<pre>
# help cmake find the Irrlicht headers library
export CMAKE_LIBRARY_PATH=$HOME/Development/irrlicht-1.5/lib/MacOSX:$CMAKE_LIBRARY_PATH
export CMAKE_INCLUDE_PATH=$HOME/Development/irrlicht-1.5/include:$CMAKE_INCLUDE_PATH

# help cmake find the IrrKlang headers library
export CMAKE_LIBRARY_PATH=$HOME/Development/irrKlang-1.1.3/bin/macosx-gcc:$CMAKE_LIBRARY_PATH
export CMAKE_INCLUDE_PATH=$HOME/Development/irrKlang-1.1.3/include:$CMAKE_INCLUDE_PATH
export DYLD_LIBRARY_PATH=$HOME/Development/irrKlang-1.1.3/bin/macosx-gcc:$DYLD_LIBRARY_PATH</pre>

				if both directories were unzipped to <tt>$HOME/Development</tt>. <tt>DYLD_LIBRARY_PATH</tt> must be set to find the dynamic irrKlang library at startup.</li>
				<li>Create a build directory (cmake should always tun out-of-source), e.g. in the <tt>cerberus</tt> folder run: <tt>mkdir build; cd build</tt>
				<li>then start the cmake configuration stage with: <tt>cmake -DCMAKE_BUILD_TYPE=Debug</tt> .. or <tt>cmake -DCMAKE_BUILD_TYPE=Release ..</tt></li>
				<li>create the executable by running <tt>make</tt></li>
				<li>now copy the cerberus.cfg from the main directory, e.g. <tt>cp ../cerberus.cfg .</tt>
				    and run cerberus <tt>./cerberus</tt></li>
	</ul>
	<h3>Linux</h3>
	N/A
	<h3>Windows</h3>
	N/A

	<h2>change.log.</h2>

	<h3>v0.2.1</h3>
	<ul>
		<li>It�s possible to win the game by hyperjumping from the pedestal.</li>
		<li>Updated to latest irrKlang and irrLicht libraries.</li>
		<li>Added music from Nine Inch Nails</li>
		<li>Game logic for cerberus was added.</li>
		<li>The pedestal and cerberus were added.</li>
	</ul>

	<h3>v0.2</h3>
	<ul>
		<li>Pressing "u" will turn the view 180 deg.</li>
		<li>Source code is now documented with <a href="http://www.doxygen.org">Doxygen</a>.</li>
		<li>In Mac OS X a complete app bundle is created with irrKlang library and media files included.</li>
		<li>The movement of the player in the landscape is now fully implemented. The player can
		    stack boulders and put a robot on top to gain height.</li>
	</ul>

	<h3>v0.1</h3>
	<ul>
		<li>It's possible to create trees, boulders and robots and transfer the
		    the soul into another robot.</li>
		<li>Source compiles on Mac OS X, Windows XP and Linux (using Irrlicht rendering
			  engine and Irrklang 3D Audio engine).</li>
	</ul>
</div>

<!-- include the page footer -->
<?php pageFooter(); ?>

</body>
</html>
