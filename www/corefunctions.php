<?php
	//
	// Print standard page header
	//
	function pageHeader( $title="main" )
	{
		echo <<<END
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<!-- This template can be downloaded from www.openwebdesign.org. Just search for neuphoric -->
<!-- This template has been modified by mejobloggs (http://www.openwebdesign.org/userinfo.phtml?user=mejobloggs) -->
<!-- Please use this template according to the license on the original template (included as pleaseread.txt) -->
<!-- The original of this template was designed by http://www.tristarwebdesign.co.uk - please visit for more templates & information - thank you. -->

<head>
  <title>cerberus - $title</title>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
  <meta name="description" content="Homepage of the 'The Sentinel' remake cerberus." />
  <meta name="keywords" content="sentinel,game,irrlicht,c++" />
  <link rel="stylesheet" type="text/css" href="css/styles.css" />
  <script type="text/javascript" src="js/slimbox.js"></script>
  <link rel="stylesheet" href="css/slimbox.css" type="text/css" media="screen" />
</head>
END;
	}

	//
	// Print menu code with selected element
	// $item might be: index, download, manual, screenshots
	//
	function pageMenu( $item )
	{
		echo "<div id=\"wrapper-header\">";
		echo "  <div id=\"header\">";
		echo "    <h1>cerberus.$item</h1>";
		echo "  </div>";
		echo "</div>";

		echo "<div id=\"wrapper-menu\">";
		echo "  <div id=\"menu\">";
		echo "    <ul>";
		echo "      <li><a title=\"main page\" href=\"index.php\">main.</a></li>";
		echo "      <li><a title=\"screenshots\" href=\"screenshots.php\">screenshots.</a></li>";
		echo "      <li><a title=\"manual\" href=\"manual.php\">manual.</a></li>";
		echo "      <li><a title=\"download\" href=\"download.php\">download.</a></li>";
		echo "    </ul>";
		echo "  </div>";
		echo "</div>";
	}

	//
	// Print a standard page footer
	//
	function pageFooter()
	{
    echo "<div id=\"footer\">";
    echo "  original design by <a title=\"derby web design\" href=\"http://www.tristarwebdesign.co.uk\">tri-star web design</a>";
    echo "</div>";
		echo <<<END    
<!-- Piwik -->
<script type="text/javascript">
var pkBaseURL = (("https:" == document.location.protocol) ? "https://www.miscdebris.net/piwik/" : "http://www.miscdebris.net/piwik/");
document.write(unescape("%3Cscript src='" + pkBaseURL + "piwik.js' type='text/javascript'%3E%3C/script%3E"));
</script><script type="text/javascript">
try {
var piwikTracker = Piwik.getTracker(pkBaseURL + "piwik.php", 2);
piwikTracker.trackPageView();
piwikTracker.enableLinkTracking();
} catch( err ) {}
</script><noscript><p><img src="http://www.miscdebris.net/piwik/piwik.php?idsite=2" style="border:0" alt=""/></p></noscript>
<!-- End Piwik Tag -->
END;
	}
?>
