#!/bin/sh

DESTINATION=$HOME/Sites/cerberus
rm -rf $DESTINATION
mkdir -p $DESTINATION

cp -r css $DESTINATION
cp -r images $DESTINATION
cp -r js $DESTINATION
cp -r screenshots $DESTINATION

cp index.php $DESTINATION
cp manual.php $DESTINATION
cp download.php $DESTINATION
cp screenshots.php $DESTINATION
cp corefunctions.php $DESTINATION
