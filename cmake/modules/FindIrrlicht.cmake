# Find irrlicht header and library (tested with version 1.5)

# This module defines the following uncached variables:
#  IRRLICHT_FOUND, if false, do not try to use irrlicht
#  IRRLICHT_INCLUDE_DIRS, where to find irrlicht.h.
#  IRRLICHT_LIBRARIES, the libraries to link against to use Irrlicht
#  IRRLICHT_LIBRARY_DIRS, the directory where the Irrlicht library is found.

set(IRRLICHT_FOUND OFF)

find_path(
  IRRLICHT_INCLUDE_DIR
	  irrlicht.h 
  	/usr/local/include 
  	/usr/include
)

if(IRRLICHT_INCLUDE_DIR)
  find_library(IRRLICHT_LIBRARY
    NAMES Irrlicht
    PATHS /usr/local/lib /usr/lib
  )
  if(IRRLICHT_LIBRARY)
    get_filename_component(IRRLICHT_LIBRARY_DIRS ${IRRLICHT_LIBRARY} PATH)

    # Set uncached variables as per standard.
    set(IRRLICHT_FOUND ON)
    set(IRRLICHT_INCLUDE_DIRS ${IRRLICHT_INCLUDE_DIR})
    set(IRRLICHT_LIBRARIES ${IRRLICHT_LIBRARY})
    if(APPLE)
			#FIND_LIBRARY(COCOA_LIBRARY Cocoa)
			FIND_LIBRARY(CARBON_LIBRARY Carbon)
			FIND_LIBRARY(FOUNDATION_LIBRARY Foundation)
			FIND_LIBRARY(APPKIT_LIBRARY AppKit)
			FIND_LIBRARY(IOKIT_LIBRARY IOKit)
	    set(IRRLICHT_LIBRARIES 
	    	${IRRLICHT_LIBRARIES}
			#	${COCOA_LIBRARY}
	    	${CARBON_LIBRARY}
	    	${FOUNDATION_LIBRARY}
	    	${APPKIT_LIBRARY}
	    	${IOKIT_LIBRARY}
	   )
		endif(APPLE)
  endif(IRRLICHT_LIBRARY)
endif(IRRLICHT_INCLUDE_DIR)
	    
if(IRRLICHT_FOUND)
  if(NOT Irrlicht_FIND_QUIETLY)
    message(STATUS "FindIrrlicht: Found both Irrlicht headers and library")
  endif(NOT Irrlicht_FIND_QUIETLY)
else(IRRLICHT_FOUND)
  if(Irrlicht_FIND_REQUIRED)
    message(FATAL_ERROR "FindIrrlicht: Could not find Irrlicht headers or library")
  endif(Irrlicht_FIND_REQUIRED)
endif(IRRLICHT_FOUND)

MARK_AS_ADVANCED(
  IRRLICHT_INCLUDE_DIR
  IRRLICHT_LIBRARY
)
