
################################################################
# Project Cerberus
#
# run cmake with:
# cmake -DCMAKE_INSTALL_PREFIX=./local \
#       -DCMAKE_BUILD_TYPE=Release ..
################################################################

project(cerberus)

# version number
set(cerberus_MAJOR 0)
set(cerberus_MINOR 2)
set(cerberus_PATCH 1)
set(cerberus_VERSION ${cerberus_MAJOR}.${cerberus_MINOR}.${cerberus_PATCH})
set(cerberus_DESCRIPTION "The Sentinel remake")

# Minimum cmake version required
cmake_minimum_required(VERSION 2.4.7)

#if(COMMAND cmake_policy)
#	cmake_policy(SET CMP0003 NEW)
#endif(COMMAND cmake_policy)

# Location where cmake build system first looks for cmake modules
set(CMAKE_MODULE_PATH ${PROJECT_SOURCE_DIR}/cmake/modules)


################################################################
# Setting paths and compiler flags
################################################################

# find OpenGL headers and library
find_package(OpenGL REQUIRED)
include_directories(${OPENGL_INCLUDE_DIR})

# find Irrlicht headers and library
find_package(Irrlicht REQUIRED)
include_directories(${IRRLICHT_INCLUDE_DIRS})
if(UNIX AND NOT APPLE)
	include_directories(/usr/X11R6/include)
endif(UNIX AND NOT APPLE)
if(APPLE)
  add_definitions(-DMACOSX)
endif(APPLE)

# find IrrKlang headers and library if available
find_package(IrrKlang)
if(IrrKlang_FOUND)
  include_directories(${IrrKlang_INCLUDE_DIRS})
  add_definitions(-DUSE_IRRKLANG)
endif(IrrKlang_FOUND)

# the zip program is needed to produce the base.pk3 data package
find_program(ZIP_FOUND zip)
if(NOT ZIP_FOUND)
  message("-- Looking for zip - not found")
  message("--   zip is required to build the data container base.pk3")
  message("--   base.pk3 will not be build")
else(NOT ZIP_FOUND)
  message("-- Looking for zip - found")
endif(NOT ZIP_FOUND)

# for debug build add _DEBUG macro
set_property(DIRECTORY APPEND
	PROPERTY COMPILE_DEFINITIONS_DEBUG _DEBUG)


################################################################
# Configure the actual program
################################################################
add_subdirectory(src)
	
	
################################################################
# Make source and binary packages
################################################################

# CPack version numbers for release tarball name.
set(CPACK_PACKAGE_VERSION_MAJOR ${cerberus_MAJOR})
set(CPACK_PACKAGE_VERSION_MINOR ${cerberus_MINOR})
set(CPACK_PACKAGE_VERSION_PATCH ${cerberus_PATCH})

#set(CPACK_PACKAGE_DESCRIPTION_FILE ${CMAKE_CURRENT_SOURCE_DIR}/README)

set(CPACK_PACKAGE_VENDOR "Werner Smekal")
if(WIN32)
  set(CPACK_GENERATOR ZIP)
	set(CPACK_SOURCE_GENERATOR ZIP)
else(WIN32)
	if(APPLE)
		# To Create a package, one can run "cpack -G DragNDrop CPackConfig.cmake" on Mac OS X
		# where CPackConfig.cmake is created by including CPack
		# And then there's ways to customize this as well
		set(CPACK_DMG_VOLUME_NAME "Cerberus Game")
		#set(CPACK_DMG_DS_STORE "misc/.DS_Store")
		#set(CPACK_DMG_BACKGROUND_IMAGE "")
		set(CPACK_BINARY_DRAGNDROP ON)
		set(CPACK_GENERATOR DragNDrop)
		set(CPACK_SOURCE_GENERATOR TGZ)		
	else(APPLE)
		set(CPACK_GENERATOR TGZ)
		set(CPACK_SOURCE_GENERATOR TGZ)
	endif(APPLE)
endif(WIN32)

# Ignore the following files/directories
set(
  CPACK_SOURCE_IGNORE_FILES
    "/\\\\.svn/"
    "\\\\.DS_Store"
    "^${PROJECT_SOURCE_DIR}/Notes.txt"
    "^${PROJECT_SOURCE_DIR}/www/"
    "^${PROJECT_SOURCE_DIR}/build/"
    "^${PROJECT_SOURCE_DIR}/debug/"
    "^${PROJECT_SOURCE_DIR}/docs/"
    "^${PROJECT_SOURCE_DIR}/miscdebris/"
    "^${PROJECT_SOURCE_DIR}/release/"
    "^${PROJECT_SOURCE_DIR}/xcode/"
)

include(CPack)

################################################################
# summary
################################################################

message( "" )
message( "Summary:" )
message( "  CMAKE_SYSTEM_NAME = ${CMAKE_SYSTEM_NAME}" )
message( "  WIN32 = ${WIN32}" )
message( "  UNIX = ${UNIX}" )
message( "  APPLE = ${APPLE}" )
message( "  CMAKE_BUILD_TYPE = ${CMAKE_BUILD_TYPE}" )
message( "  CMAKE_INSTALL_PREFIX = ${CMAKE_INSTALL_PREFIX}" )
message( "  CMAKE_OSX_ARCHITECTURES = ${CMAKE_OSX_ARCHITECTURES}" )
message( "  CMAKE_GENERATOR = ${CMAKE_GENERATOR}" )
message( "" )
	