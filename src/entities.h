
#ifndef __ENTITIES_H
#define __ENTITIES_H

// Include irrlicht headers
#include "irrlicht.h"

// Forward declarations
class cWorld;
class cSoul;

// Enumerations
enum eEntity { eNil=0, eElement=1, eTree, eBoulder, eRobot, ePedestal, eMeanie, eSentry, eSentinel  };
enum eEnergy { eNilNrg=0, eTreeNrg=1, eBoulderNrg=2, eRobotNrg=3, eMeanieNrg=4, eSentryNrg=5, eSentinelNrg=6 };
enum eStatus { eDestroyed=-1, eIdle=0, eVanish, eRotate, eWatch, eAbsorb };

/**
 *
 */
class cEntityBase {
public:
  cEntityBase( eEntity type, cWorld* world, IrrlichtDevice* device ) :
			mWorld( world ), mDevice( device ), mPreviousEntity( NULL ),
			mNextEntity( NULL ), mType( type ), mStatus(eIdle)
  	{ }
  virtual ~cEntityBase()
    { }
	
  void setIndex( int i, int j )
    { mIndexI=i; mIndexJ=j; }
  int getIndexI()
    { return mIndexI; }
  int getIndexJ()
    { return mIndexJ; }
	void setGroundLevel( f32 groundLevel )
		{ mGroundLevel = groundLevel; }
	f32 getGroundLevel()
		{ return mGroundLevel; }
	void setHeight( f32 height )
		{ mHeight = height; }
	f32 getHeight()
		{ return mHeight; }


	cEntityBase* getPreviousEntity()
		{ return mPreviousEntity; }
	void setPreviousEntity( cEntityBase* entity )
		{ mPreviousEntity = entity; }
	cEntityBase* getNextEntity()
		{ return mNextEntity; }
	void setNextEntity( cEntityBase* entity )
		{ mNextEntity = entity; }

	int getEnergy();
	eEntity getType()
		{ return mType; }
  cEntityBase* createEntity( eEntity entity );
	
	void setStatus(eStatus status)
		{  mStatus=status; }
  eStatus getStatus()
		{ return mStatus; }
		
	virtual scene::ISceneNode* getNode() = 0;
	virtual eStatus processEvents(u32 time) = 0;
  
protected:
  cWorld* mWorld;
  IrrlichtDevice* mDevice;

	int mIndexI;
	int mIndexJ;
	f32 mGroundLevel;
	f32 mHeight;

	cEntityBase* mPreviousEntity;
	cEntityBase* mNextEntity;

	eEntity mType;
  eStatus mStatus;
};


/*!
 *
 */
class cEntity : public cEntityBase {
public:
  cEntity(eEntity type, scene::IAnimatedMeshSceneNode* Node, cWorld* world, IrrlichtDevice* device);
	~cEntity();
	
	void setNode(scene::IAnimatedMeshSceneNode* Node)
		{ mNode=Node; }
	scene::ISceneNode* getNode()
		{ return mNode; }

	virtual eStatus processEvents(u32 time);
	  
protected:
  scene::IAnimatedMeshSceneNode* mNode;
	u32 mLastProcessTime;
	f32 mScale;
	f32 mVanishScaleSpeed;
	f32 mVanishRotateSpeed;
};


/*!
 *
 */
class cCerberus : public cEntity {
public:
  cCerberus(eEntity type, scene::IAnimatedMeshSceneNode* Node, cWorld* world, IrrlichtDevice* device);
	~cCerberus();

	eStatus processEvents(u32 time);
	void updateFieldOfView();
	bool checkFieldOfView();

private:	
	u32 mLastRotateTime;
	u32 mWatchPeriod;
	u32 mLastAbsorbTime;
	u32 mRecoverPeriod;
	f32 mRotateSpeed;
	f32 mRotationStep;
	f32 mPhi;
	bool** mView;     //!< land information
	int mWorldSize;
};


/*!
 *
 */
class cElement : public cEntityBase {
public:
	cElement( cWorld* world, IrrlichtDevice* device );
	~cElement();
    
  void setLevel( int level )
    { mLevel=level; mGroundLevel=level*sqrt(2.0); }
  int getLevel()
    { return mLevel;}
  core::vector3df getPosition()
    { return core::vector3df( getIndexI()+0.5, mGroundLevel, getIndexJ()+0.5 ); }
  
  void setNode( scene::IMeshSceneNode* Node )
    { mNode=Node; }
  scene::ISceneNode* getNode()
    { return mNode; };
    
  void setFlat( bool flat )
    { mFlat=flat; }
  bool isFlat()
    { return mFlat; }

	virtual eStatus processEvents(u32 time)
		{ return mStatus; }
		
private:
  ILogger *mLogger;
  scene::IMeshSceneNode* mNode;
	int mLevel;
  bool mFlat;
};


/**
 *
 */
class cSoul {
public:
  // constructor
  cSoul( scene::ICameraSceneNode* camera );

  void changeView( f32 px, f32 py );  
  void turnAround();  
  void updateView();  

	// position of soul
  void setIndex( int i, int j )
    { mIndexI=i; mIndexJ=j; }
	void setGroundLevel( f32 groundLevel )
		{ mGroundLevel = groundLevel; }
	f32 getGroundLevel()
		{ return mGroundLevel; }
  int getIndexI()
    { return mIndexI;}
  int getIndexJ()
    { return mIndexJ;}
  core::vector3df getPosition()
    { return core::vector3df( mIndexI+0.5, mGroundLevel+1.1, mIndexJ+0.5 ); }
	void setTarget( core::vector3df target );

  // manage energy of soul
  int getEnergy()
    { return mEnergy; }
  void addToEnergy( int add )
    { mEnergy += add; }
  bool substractFromEnergy( int sub );
    
  // manage container=robot of soul
  cEntityBase* getRobot()
  	{ return mRobot; }
  void setRobot( cEntityBase* robot )
  	{ mRobot = robot; }
    
	void setSeenFlag(bool flag)
		{ mSeenFlag = flag; }
	bool isSeen()
	{ return mSeenFlag; }
	
private:
  scene::ICameraSceneNode* mCamera;
  f32 mTheta, mPhi;
  const f32 mThetaMin, mThetaMax;    
  f32 mRotateSpeed;
  
  int mIndexI, mIndexJ;
  f32 mGroundLevel;
  int mEnergy;
	bool mSeenFlag;
  
  cEntityBase* mRobot;
};

#endif // __ENTITIES_H
