/*
 * lua_config provides some helper functions to get variables from 
 * the global lua name space or from a given table. This should
 * help to read out a lua config file.
 *
 * Author: Werner Smekal, 2007
 */

/*****************************************************************************
  lua headers and macros
 *****************************************************************************/

#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>


/*****************************************************************************
  lua configuration helper functions
 *****************************************************************************/

int get_boolean( lua_State* L, const char* name, const int mDefault );
double get_number( lua_State* L, const char* name, const double mDefault );
void get_string( lua_State* L, const char* name, char* buffer, size_t bufferSize, const char* mDefault );

int get_boolean_from_table( lua_State* L, const char* table, const char* name, const int mDefault );
double get_number_from_table( lua_State* L, const char* table, const char* name, const double mDefault  );
void get_string_from_table( lua_State* L, const char* table, const char* name, char* buffer, size_t bufferSize, 
                            const char* mDefault );

void call_lua_function( lua_State* L, const char *func, const char *sig, ... );

