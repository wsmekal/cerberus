
// include stl headers
#include <iostream>
#include <string>
#include <cstdio>
#include <cstdarg>

// lua headers
extern "C" {
	#include <lua.h>
	#include <lualib.h>
	#include <lauxlib.h>
	
	#include "lua_config.h"
}

// Mersenne Twister RNG
#include "mt19937.hpp"

// include irrlicht headers
#include "irrlicht.h"
#include "irrConsole/console.h"
using namespace irr;

// cerberus headers
#include "landscape.h"
#include "cerberus.h"
#include "menu.h"
#include "game.h"


#ifdef __APPLE__
	#include "CoreFoundation/CoreFoundation.h"
#endif // __APPLE__

  
  // printf-like function
char* formatString( char* format, ... )
{
  static char tmpBuffer[4096];
  tmpBuffer[0]='\0';
  
  va_list args;
  va_start( args, format );
  vsnprintf( tmpBuffer, sizeof(tmpBuffer), format, args );
  va_end( args );
  
  return tmpBuffer;
}
  

void test_mt19937( void )
{
  const unsigned int no_bits = 23000000;
  FILE* fh; 

  fprintf( stdout, "Writing 23 million bits to the file 'mt19937.bit'.\n" );
  fprintf( stdout, "Run the diehard tests on this file.\n" );

  // seed
  //unsigned long init[4]={ 0x123, 0x234, 0x345, 0x456 }, length=4;
  //mt19937* rng = new mt19937( init, length );  
  mt19937* rng = new mt19937( ((unsigned long)(0)+8749UL)*23095UL );

  // open file and write random numbers to it
  fh=fopen( "mt19937.bit", "wb" );
  
  unsigned long random_number;
  for( unsigned int i=0; i<no_bits; i++ ) {
    random_number = rng->genrand_int32();
    fwrite( &random_number, 4, 1, fh );
  }
  
  fclose( fh );
  delete rng;

  fprintf( stdout, "Done, exiting.\n" );
}


/*!
 * entry function of program
 * 
 * @param argc : number of arguments
 * @param argv : list of arguments
 */
int main( int argc, char* argv[] )
{
	unsigned int level=0;
	std::string mediaPath="."; 

#ifdef __APPLE__
	// This makes relative paths work in C++ in Xcode by determining the Resources path inside the .app bundle
		const size_t pathSize = 1024;
	char path[pathSize];

  CFBundleRef mainBundle = CFBundleGetMainBundle();
	CFURLRef resourcesURL = CFBundleCopyResourcesDirectoryURL( mainBundle ); 
  if( !CFURLGetFileSystemRepresentation(resourcesURL, TRUE, (UInt8 *)path, pathSize) ) {
		puts("Unable to determine Resources path in app bundle");
		exit(-1);
	}
	CFRelease(resourcesURL);

	mediaPath = path;
	std::cout << "Media Path: " << mediaPath << std::endl;
#endif  // __APPLE__

	// test RNG if the user calls cerberus with 'testrng' as option
  // get all landscape codes with 'iwantthelandscapecodesplease' as options
  if( argc==2 ) {
    if( strcmp(argv[1], "testrng")==0 ) {
      test_mt19937();
      exit( 0 );
    } else if( strcmp(argv[1], "iwantthelandscapecodesplease")==0 ) {
      FILE* fh; 

      fh=fopen( "landscape_codes.txt", "w" );
      mt19937* rng = new mt19937( 0 );
      for( int level=1; level<10000; level++ ) {
        rng->init( ((unsigned long)(level)+8749UL)*23095UL );
        fprintf( fh, "%04d %08d\n", level, (unsigned int)(rng->genrand_real1()*100000000.0) );
      }
      delete rng;
      fclose( fh );
        
      exit( 0 );
    } else {
			fprintf( stderr, "Parameter '%s' unknown!\n", argv[1] );
		}
  } else if( argc==3 ) {
    if( strcmp(argv[1], "level")==0 ) {
			level = (unsigned int)atoi( argv[2] );
			if( level>9999 ) {
				fprintf( stderr, "'level'(=%s) must be a number from 0 to 9999!\n", argv[2] );
				exit( -1 );
			}
		} else {
			fprintf( stderr, "Parameter '%s' unknown!\n", argv[1] );
		}
	}

  // open lua interpreter
  lua_State* luaState = lua_open();
  luaopen_base( luaState );
  luaopen_io( luaState );
  luaopen_string( luaState );
  luaopen_math( luaState );

  // set configuration
  cConfiguration mConfiguration;
  mConfiguration.loadFromFile( luaState, formatString("%s/cerberus.cfg", mediaPath.c_str()) );
  mConfiguration.max_fps=4000.;
  mConfiguration.fps_period=1000.0/mConfiguration.max_fps;
  
  // driver type (software renderer (=1) default)
	video::E_DRIVER_TYPE driverType = video::EDT_SOFTWARE;
  switch( mConfiguration.driverType ) {
  case 2: driverType = video::EDT_BURNINGSVIDEO; break;
  case 3: driverType = video::EDT_DIRECT3D8; break;
  case 4: driverType = video::EDT_DIRECT3D9; break;
  case 5: driverType = video::EDT_OPENGL; break;
  default: break;
  }

	// create device
	IrrlichtDevice* mDevice = createDevice( driverType, core::dimension2d<u32>(mConfiguration.width, mConfiguration.height),
                                          mConfiguration.screenDepth, mConfiguration.fullscreen,
                                          false, mConfiguration.vsync );
	if( mDevice == 0 ) {
    fprintf( stderr, "Couldn't create device\n" );
		exit( -1 ); // could not create selected driver.
  }

  if( !mDevice->getFileSystem()->addZipFileArchive( formatString("%s/%s", mediaPath.c_str(), "base.pk3"), true, false ) ) {
  	mDevice->getLogger()->log( "Can't open base.pk3", "base.pk3 must be in the same directory as the executable", ELL_ERROR );
  	exit( -1 );
  }
  
  // start Audio device
  cAudioDevice mAudioDevice( mediaPath );

  // initialize the console
  IC_Console mConsole( luaState );
	mConsole.getConfig().dimensionRatios.Y = 0.6f;
	mConsole.getConfig().fontName = "media/console.bmp";
	mConsole.initializeConsole( mDevice );
	mConsole.loadDefaultCommands( mDevice );

  while( true ) {
		// show menu
		if( level==0 ) {
			cMenuScreen menu( mDevice, &mConfiguration, &mAudioDevice );
			if( !menu.run(&level) )
				break;
		}
		
	  // run game
	  cGame game( mDevice, &mConfiguration, &mAudioDevice, &mConsole, level );
	  game.run();
		
		level=0;
	}
  
  // close everything
	mDevice->drop();

  // close lua interpreter
  lua_close( luaState );  
	
	return 0;
}


/*!
 * 
 */
bool cConfiguration::loadFromFile( lua_State* L, const char* filename )
{
  // open configuration file
  if( luaL_loadfile(L, filename) || lua_pcall(L, 0, 0, 0) )
    printf( "cannot run configuration file: %s", lua_tostring(L, -1) );
  
  width          = get_number( L, "width", 640 );
  height         = get_number( L, "height", 400 );
  fullscreen     = get_boolean( L, "fullscreen", false );
  screenDepth    = (u32)get_number( L, "screenDepth", 16 );
  vsync          = get_boolean( L, "vsync", false );
  driverType     = (size_t)get_number( L, "driverType", 1 );
  
  return true;
}

