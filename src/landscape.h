

#ifndef __LANDSCAPE_H
#define __LANDSCAPE_H

// standard headers
#include <iostream>
#include <cmath>

// Mersenne Twister RNG
#include "mt19937.hpp"

// irrlicht headers
#include "irrlicht.h"
using namespace irr;

// cerberus headers
#include "entities.h"
#include "cerberus.h"


class cWorld {
public:
  cWorld( IrrlichtDevice* device, cAudioDevice* audioDevice, int level, int size, int energy, double scalarStart, double hScalar );
  ~cWorld();
	
  void print();
  void create();
  void showStartScreen();
  scene::ISceneNode* getNode()  { return mNode; }
  cSoul* getSoul()  { return mSoul; }
	void CheckSelection();
	
	int getSize()
		{ return mSize; }
  
  scene::IAnimatedMesh* getTreeMesh()
    { return mTreeMesh; }
  scene::IAnimatedMesh* getBoulderMesh()
    { return mBoulderMesh; }
  scene::IAnimatedMesh* getRobotMesh()
    { return mRobotMesh; }
  scene::IAnimatedMesh* getPedestalMesh()
    { return mPedestalMesh; }
  scene::IAnimatedMesh* getSentinelMesh()
    { return mSentinelMesh; }
	core::stringw GetName()
		{ return mName; }

	scene::ISceneManager* getSceneManager()
		{ return mSmgr; }

	cEntityBase* cWorld::findEntity();
	bool createEntity( eEntity entityType, cEntityBase* entityBelow );
	int absorbEntity( cEntityBase* entityBelow );  /**< Absorb entity above entityBelow */
	bool hyperjump();
	bool doTransferSoul( cEntityBase* robot );
	bool transferSoul( cEntityBase* entityBelow );
	void rotateCerberus( cEntityBase* cerberus );
	void createTree();
	cEntityBase* checkForSources(bool** View);
	
  inline double getRandomNumber()
    { return rng->genrand_real1(); }
		
	// run game logic and other processes 
	void processEvents(u32 time);
		
  // static function to check code
  static int getLandscapeCode(unsigned int level);
  
  cAudioDevice* getAudioDevice()
  	{ return mAudioDevice; }
    
	bool isDefeated()
		{ return mDefeated; }
	
private:
  void init();  
  double gaussian( double x0, double sigma );
  double gaussian_4( double sigma, double a1, double a2, double a3, double a4 );
  double gaussian_3( double sigma, double a1, double a2, double a3 );
	void checkDefeated();
  
private:
  // preloaded meshes and icons 
  scene::IAnimatedMesh* mTreeMesh;       //!< Tree mesh
  scene::IAnimatedMesh* mBoulderMesh;    //!< Boulder mesh
  scene::IAnimatedMesh* mRobotMesh;      //!< Robot mesh
  scene::IAnimatedMesh* mPedestalMesh;      //!< Robot mesh
  scene::IAnimatedMesh* mSentinelMesh;      //!< Robot mesh

	IrrlichtDevice* mDevice;
  video::IVideoDriver* mDriver;
	scene::ISceneManager* mSmgr;
	gui::IGUIEnvironment* mEnv;
  ILogger *mLogger;
	cAudioDevice* mAudioDevice;
	cEventManager* mEventManager;
	
	unsigned int mLevel;   //!< which level is this landscape
  int mSize;            //!< size of land
  int mEnergy;          //!< available energy for this world
  double mScalarStart;
  double mHScalar;
  scene::ISceneNode* mNode;
  scene::ICameraSceneNode* mCamera;
  cSoul *mSoul;                    //!< robot with soul
	bool mDefeated;
	core::stringw mName;   //!< "name" of landscape, i.e. "landscape 0653"

  double** mPoints;     //!< land information
	cElement*** mElements; //!< floor elements
  mt19937* rng;          //!< pointer to RNG
};

#endif
