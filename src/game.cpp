
// include irrlicht headers
#include "irrlicht.h"
#include "irrConsole/console.h"
using namespace irr;

#include "cerberus.h"
#include "game.h"
#include "landscape.h"


/**
 * Initialize game
 */
cGame::cGame( IrrlichtDevice* device, cConfiguration* configuration, cAudioDevice* audioDevice,
              IC_Console* console, unsigned int level ) :
    mDevice( device ), mConfiguration( configuration ), mAudioDevice( audioDevice ),
    mConsole(console), mLevel( level )
{
  // set pointers to basic irrlicht objects
	mDriver = mDevice->getVideoDriver();
	mSmgr = mDevice->getSceneManager();
	mEnv = mDevice->getGUIEnvironment();
	mLogger = mDevice->getLogger();  
  
  // load icons/logos/textures
	mDriver->setTextureCreationFlag( video::ETCF_ALWAYS_32_BIT, true );
  tIrrlichtLogo = mDriver->getTexture( "media/irrlichtlogo2.png" );
  tNrgTree      = mDriver->getTexture( "media/tree_energy.png" );
  tNrgBoulder   = mDriver->getTexture( "media/boulder_energy.png" );
  tNrgRobot     = mDriver->getTexture( "media/robot_energy.png" );
  tNrgRobot5    = mDriver->getTexture( "media/robot_energy5.png" );
  
	// main random number generator
  // each world has its own generator
  mRNG = new mt19937( 0 );  

	// add irrlicht logo
	mEnv->addImage( tIrrlichtLogo, core::position2d<s32>(10, mConfiguration->height-92) );

	// catch and disable mouse cursor
  mDevice->getCursorControl()->setPosition( 0.5f, 0.5f );
	mDevice->getCursorControl()->setVisible( false );

  // create world
  mWorld = new cWorld( mDevice, mAudioDevice, mLevel, 33, 15, 4, 0.9 );
  mWorld->create();
  if( mWorld->getNode() ) {
    mWorld->getNode()->setPosition( core::vector3df(0, 0, 0) );
    mWorld->getNode()->setRotation( core::vector3df(0, 0, 0) );
  }

	// add sky (scene::ISceneNode* skynode=)
	mSmgr->addSkyDomeSceneNode( mDriver->getTexture("media/alien_world.jpg"), 16, 16, 1.0, 2.0 );

	// attach event receiver
	mDevice->setEventReceiver( this );

  // Game starts with a view of the world
  mGameStatus=eGameStart;
  mWorld->showStartScreen();
  
  // play game music
  mAudioDevice->playMusic( "music/NIN-Corona_Radiata.ogg" );
  
  // reset virtual timer
  mDevice->getTimer()->setTime( 0 );
	mDevice->getTimer()->setSpeed( 1.0 );
}


/*!
 * Free memory and handles
 */
cGame::~cGame( void )
{
	// delete allocated objects
	if( mWorld )
		delete mWorld;
	delete mRNG;

	// remove event receiver
	mDevice->setEventReceiver( NULL );
	
	// clear SceneManager
	mSmgr->clear();
}


/*!
 * Event handler
 */
bool cGame::OnEvent( const SEvent& event )
{
  switch( mGameStatus ) {
  case eGameStart:
    // process keys events
    if( event.EventType == EET_KEY_INPUT_EVENT && !event.KeyInput.PressedDown ) {
      switch (event.KeyInput.Key) {
      case KEY_ESCAPE: // go back to menu
				mGameStatus=eGameQuit;
        return true;
      case KEY_KEY_S: // take and save screenshot
        mDriver->writeImageToFile( mDriver->createScreenShot(), "screenshot.png" );
        return true;
			default:
				mWorld->getSoul()->updateView();
				//mDriver->setFog(video::SColor(0,0,0,0), true, 1, 50, 100, false);
				mGameStatus=eGameRuns;
				return true;
			}
		}

    // process mouse events
    if( event.EventType == EET_MOUSE_INPUT_EVENT ) {
      switch( event.MouseInput.Event ) {
      case EMIE_LMOUSE_PRESSED_DOWN:
				mWorld->getSoul()->updateView();
				mGameStatus=eGameRuns;
        return true;
      default:
        return true;
      }
    }
    break;
  case eGameRuns:
    // process keys events
    if( event.EventType == EET_KEY_INPUT_EVENT && !event.KeyInput.PressedDown ) {
      if( event.KeyInput.Key==KEY_TAB ) {
				if( !mConsole->isVisible() )
					mConsole->setVisible( true );
				else if( !event.KeyInput.Control )
					mConsole->setVisible( false );
				return true;
			} else if( event.KeyInput.Key==KEY_ESCAPE ) { // go back to menu
				if( mConsole->isVisible() )
					mConsole->setVisible( false );
				else
          mGameStatus=eGameQuit;
        return true;
      }

      if( mConsole->isVisible() ) {
				mConsole->handleKeyPress( event.KeyInput.Char, event.KeyInput.Key, event.KeyInput.Shift, event.KeyInput.Control );
				return true;
			}
      
      cEntityBase* entityBelow;
      switch( event.KeyInput.Key ) {
      case KEY_KEY_S: // take and save screenshot
        mDriver->writeImageToFile( mDriver->createScreenShot(), "screenshot.png" );
        return true;
      case KEY_KEY_T: // plant a tree
				entityBelow = mWorld->findEntity();
				if( entityBelow )
					if( mWorld->createEntity( eTree, entityBelow ) )
						mLogger->log( "Planted a tree", ELL_INFORMATION );
        return true;
      case KEY_KEY_B: // create a boulder
				entityBelow = mWorld->findEntity();
				if( entityBelow )
					if( mWorld->createEntity( eBoulder, entityBelow ) )
						mLogger->log( "Planted a boulder", ELL_INFORMATION );
        return true;
      case KEY_KEY_R: // create a robot
				entityBelow = mWorld->findEntity();
				if( entityBelow )
					if( mWorld->createEntity( eRobot, entityBelow ) )
						mLogger->log( "Planted a robot", ELL_INFORMATION );
        return true;
      case KEY_KEY_H: // Hyperspace
        if(mWorld->hyperjump()) {
					// calculate new level
					unsigned int level = mLevel+1+mWorld->getSoul()->getEnergy()/3;
					printf("New level %u, code=%d\n", level, cWorld::getLandscapeCode(level));
					
					mPlayerHasWon = true;
				}
        return true;
      case KEY_KEY_A: // Absorb entity
				entityBelow = mWorld->findEntity();
				if( entityBelow )
	        mWorld->absorbEntity( entityBelow );
        return true;
      case KEY_KEY_O: // Options
        mLogger->log( "Options", ELL_INFORMATION );
        return true;
      case KEY_KEY_Q: // Transmit robot
				entityBelow = mWorld->findEntity();
				if( entityBelow )
	        mWorld->transferSoul( entityBelow );
        return true;
      case KEY_KEY_U: // Turn around
        mWorld->getSoul()->turnAround();
        return true;
      default:
        return true;			
      }
    }
    
    // process mouse events
    if( event.EventType == EET_MOUSE_INPUT_EVENT ) {
      cEntityBase* entityBelow;
      switch( event.MouseInput.Event ) {
      case EMIE_MOUSE_MOVED: {
          gui::ICursorControl* cursorcontrol = mDevice->getCursorControl();
          core::position2d<f32> pos = cursorcontrol->getRelativePosition();
        
          if( (pos.X!=0.5f) || (pos.Y!=0.5f) ) {
            mWorld->getSoul()->changeView( pos.X-0.5f, pos.Y-0.5f );
            // reset mouse pointer so that we don't loose focus
            cursorcontrol->setPosition( 0.5f, 0.5f );
          }
          return true;
        }
      case EMIE_LMOUSE_PRESSED_DOWN:
				entityBelow = mWorld->findEntity();
				if( entityBelow )
	        mWorld->absorbEntity( entityBelow );
        return true;
      case EMIE_RMOUSE_PRESSED_DOWN:
				entityBelow = mWorld->findEntity();
				if( entityBelow )
	        mWorld->transferSoul( entityBelow );
        return true;
      default:
        return true;
      }
      
    }
    
		if( event.EventType == EET_LOG_TEXT_EVENT ) {
			mConsole->logMessage( event.LogEvent.Level, event.LogEvent.Text );
			return true;
		}
    break;
  case ePlayerIsDead:
    break;
  case ePlayerHasWon:
    break;    
	case eGameQuit:
		// the loop will drop out before, so we do nothing here
		break;
  }

  return false;
}


/*!
 * Draw the 2D GUI Interface on top of the 3D view
 */
void cGame::drawInterface( void )
{
	// draw crosshair
  mDriver->draw2DRectangle( video::SColor(80,255,255,255),
  													core::rect<s32>(mConfiguration->width/2-20,mConfiguration->height/2-1,
  													                mConfiguration->width/2-5,mConfiguration->height/2+1) );
  mDriver->draw2DRectangle( video::SColor(80,255,255,255),
  													core::rect<s32>(mConfiguration->width/2+5,mConfiguration->height/2-1,
  													                mConfiguration->width/2+20,mConfiguration->height/2+1) );
  mDriver->draw2DRectangle( video::SColor(80,255,255,255),
  													core::rect<s32>(mConfiguration->width/2-1,mConfiguration->height/2-20,
  													                mConfiguration->width/2+1,mConfiguration->height/2-5) );
  mDriver->draw2DRectangle( video::SColor(80,255,255,255),
  													core::rect<s32>(mConfiguration->width/2-1,mConfiguration->height/2+5,
  													                mConfiguration->width/2+1,mConfiguration->height/2+20) );

  // draw energy information
  int energy=mWorld->getSoul()->getEnergy();
  int xPos=10;
  int nrIcons;

  // black box at top
  mDriver->draw2DRectangle( video::SColor(255,0,0,0), core::rect<s32>(0,0,mConfiguration->width,17) );
  
  // golden robots (worth 15 energy units)
  nrIcons = (int)floor(energy/15.0);
  energy -= nrIcons*15;
  for( int i=0; i<nrIcons; i++ ) {
    mDriver->draw2DImage( tNrgRobot5, core::position2d<s32>(xPos, 2) );
    xPos+=32;
  }
  
  // robots (worth 3 energy units)
  nrIcons = (int)floor(energy/3.0);
  energy -= nrIcons*3;
  for( int i=0; i<nrIcons; i++ ) {
    mDriver->draw2DImage( tNrgRobot, core::position2d<s32>(xPos, 2) );
    xPos+=32;
  }
  
  // boulders  (worth 2 energy units)
  nrIcons = (int)floor(energy/2.0);
  energy -= nrIcons*2;
  for( int i=0; i<nrIcons; i++ ) {
    mDriver->draw2DImage( tNrgBoulder, core::position2d<s32>(xPos, 2) );
    xPos+=32;
  }
  
  // trees  (worth 1 energy unit)
  nrIcons = energy;
  for( int i=0; i<nrIcons; i++ ) {
    mDriver->draw2DImage( tNrgTree, core::position2d<s32>(xPos, 2) );
    xPos+=32;
  }
  
  // draw sentinel bar 479-614/0-11
  const int barWidth=128;
  const int barHeight=8;
  mDriver->draw2DRectangle( video::SColor(255,252,0,0),
                            core::rect<s32>(mConfiguration->width-32-barWidth-3,0,
                                            mConfiguration->width-32+3,barHeight+4) );
	mDriver->draw2DRectangle( video::SColor(255,0,0,0),
														core::rect<s32>(mConfiguration->width-32-barWidth,2,
																						mConfiguration->width-32,barHeight+2) );
                                            
  // if robot is seen by Cerberus or Meany the bar will be "energised"
  if(mWorld->getSoul()->isSeen()) {
  	unsigned long mask;
	  for( int i=0; i<barHeight; i+=2 ) {
	  	mask = mRNG->genrand_int32();
		  for( int j=0; j<barWidth/2; j+=2 ) {
		  	if( mask & (1<<j) )
					mDriver->draw2DRectangle( video::SColor(255,0,180,0),
																		core::rect<s32>(mConfiguration->width-32-barWidth+j,2+i,
																										mConfiguration->width-32-barWidth+j+2,4+i) );
	  	}
	  	if( 1 ) 
				for( int j=barWidth/2; j<barWidth; j+=2 ) {
					if( mask & (1<<j) )
						mDriver->draw2DRectangle( video::SColor(255,0,180,0),
																			core::rect<s32>(mConfiguration->width-32-barWidth+j,2+i,
																											mConfiguration->width-32-barWidth+j+2,4+i) );
				}
	  }
	}
}


/*!
 * Run the game
 */
void cGame::run( void )
{
	int lastFPS = -1, fps;
  double oldtime=0, period=0, timeset=0;
  core::stringw title = L"The Sentinel [";
  title += mDriver->getName();
  title += "] FPS: ";
  core::stringw fulltitle;
	mPlayerHasWon = false;

	gui::IGUIFont* font=mEnv->getFont("media/zeroes.xml");

  timeset=(double)(mDevice->getTimer()->getRealTime());
	while( mDevice->run() && mGameStatus!=eGameQuit ) {
    if( mDevice->isWindowActive() ) {      
      // limit output to max_fps
      period = mConfiguration->fps_period-((double)(mDevice->getTimer()->getRealTime())-oldtime);
      if( period>0 )
        mDevice->sleep( (u32)period, false );
      oldtime=(double)(mDevice->getTimer()->getRealTime());
      
      switch( mGameStatus ) {
      case eGameStart:
        // draw everything
        mDriver->beginScene( true, true, video::SColor(255, 0, 0, 0) );
        mSmgr->drawAll();
        mEnv->drawAll();
        font->draw( mWorld->GetName().c_str(), core::rect<s32>(mConfiguration->width/2, 50, mConfiguration->width/2, 50),
									  video::SColor(255,255,255,255), true, false, NULL );
        mDriver->endScene();
        break;
					
      case eGameRuns:
      	mWorld->processEvents( mDevice->getTimer()->getTime() );
      
        mDriver->beginScene( true, true, video::SColor(255, 0, 60, 156) );
            
        mSmgr->drawAll();
        mEnv->drawAll();

        // draw interface
        drawInterface();
				mConsole->renderConsole( mEnv, mDriver, 1000 );

        mDriver->endScene();

        // update 3D position for sound engine
				scene::ICameraSceneNode* cam = mSmgr->getActiveCamera();
				mAudioDevice->setListenerPosition( cam->getAbsolutePosition(), cam->getTarget() );

        // check if player is dead already (energy<=0)
        if(isPlayerDead()) {
        	// turn off fog
					//mDriver->setFog(video::SColor(0,0,0,0), true, 5, 50, 10, false);
					
					// remove world
					delete mWorld;
					mWorld=NULL;
          mSmgr->clear();
					
          mGameStatus=ePlayerIsDead;
          timeset=(double)(mDevice->getTimer()->getRealTime());
        }
					
				if(mPlayerHasWon) {
        	// turn off fog
					//mDriver->setFog(video::SColor(0,0,0,0), true, 5, 50, 10, false);

					// remove world
					delete mWorld;
					mWorld=NULL;
          mSmgr->clear();
					
          mGameStatus=ePlayerHasWon;
          timeset=(double)(mDevice->getTimer()->getRealTime());
				}
        break;
      case( ePlayerIsDead ):
        mDriver->beginScene( true, true, video::SColor(255, 0, 0, 0) );
            
        mSmgr->drawAll();
        mEnv->drawAll();

        mDriver->endScene();
        if( (double)(mDevice->getTimer()->getRealTime())-timeset>3000 )
          mGameStatus=eGameQuit;
        break;
      case ePlayerHasWon:
        mDriver->beginScene( true, true, video::SColor(255, 0, 60, 156) );
					
				mSmgr->drawAll();
				mEnv->drawAll();
				
				mDriver->endScene();
				if( (double)(mDevice->getTimer()->getRealTime())-timeset>3000 )
					mGameStatus=eGameQuit;
        break;
      case eGameQuit:
				// the loop will drop out before, so we do nothing here
				break;
      }

      // display frames per second in window title
      fps = mDriver->getFPS();
      if( lastFPS!=fps ) {
        fulltitle = title;
        fulltitle += fps;

        mDevice->setWindowCaption( fulltitle.c_str() );
        lastFPS = fps;
      } 
    }
  }
	
	// return to menu
}
