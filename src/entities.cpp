
#include "cerberus.h"
#include "entities.h"
#include "landscape.h"


/*!
 * 
 */
cEntityBase* cEntityBase::createEntity(eEntity entityType)
{
  // entity must be an element or boulder or pedestal
  if( (getType() != eElement) && (getType() != eBoulder) && (getType() != ePedestal) )
  	return NULL;

	// is this entity below another one? 
  if( getNextEntity() )
    return NULL;

	// if it's an element, it must be flat    
  if( (getType() == eElement) && !(((cElement*)this)->isFlat()) )
  		return NULL;
  
  cEntity* entity;
  switch( entityType ) {
  case eTree:
    {
      scene::IAnimatedMeshSceneNode* Node=mDevice->getSceneManager()->addAnimatedMeshSceneNode( mWorld->getTreeMesh(), mWorld->getNode() );
      if( Node ) {
        Node->setMaterialFlag( video::EMF_LIGHTING, false );
      	Node->setMaterialFlag( video::EMF_FOG_ENABLE, true );
				Node->setMaterialType(video::EMT_SOLID);
        Node->setPosition( core::vector3df(mIndexI+0.5, mGroundLevel+mHeight, mIndexJ+0.5) );
        Node->setRotation( core::vector3df(0.0, mWorld->getRandomNumber()*360.0, 0.0) );
      } else {
        fprintf( stdout, "couldn't allocate tree" );
      }
      entity = new cEntity( eTree, Node, mWorld, mDevice );
			entity->setIndex( mIndexI, mIndexJ );
			entity->setHeight( 1.767767 );
			entity->setGroundLevel( mGroundLevel+mHeight );
			setNextEntity(entity);
			entity->setPreviousEntity( this );
    }
    break;
  case eBoulder:
    {
      scene::IAnimatedMeshSceneNode* Node=mDevice->getSceneManager()->addAnimatedMeshSceneNode( mWorld->getBoulderMesh(), mWorld->getNode() );
      if( Node ) {
        Node->setMaterialFlag( video::EMF_LIGHTING, false );
				Node->setMaterialType(video::EMT_SOLID);
        Node->setPosition( core::vector3df(mIndexI+0.5, mGroundLevel+mHeight, mIndexJ+0.5) );
        Node->setRotation( core::vector3df(0.0, mWorld->getRandomNumber()*360.0, 0.0) );
      } else {
        fprintf( stdout, "couldn't allocate tree" );
      }
      entity = new cEntity( eBoulder, Node, mWorld, mDevice );
			entity->setIndex( mIndexI, mIndexJ );
			entity->setHeight( 1.0/sqrt(2.0) );
			entity->setGroundLevel( mGroundLevel+mHeight );
			setNextEntity( entity );
			entity->setPreviousEntity( this );
			scene::ITriangleSelector* entitySelector =
							mWorld->getSceneManager()->createTriangleSelector( mWorld->getBoulderMesh(), Node );
			Node->setTriangleSelector( entitySelector );
			entitySelector->drop();
    }
    break;
  case eRobot:
    {
      scene::IAnimatedMeshSceneNode* Node=mDevice->getSceneManager()->addAnimatedMeshSceneNode( mWorld->getRobotMesh(), mWorld->getNode() );
      if( Node ) {
				Node->setMaterialFlag( video::EMF_LIGHTING, false );
				Node->setPosition( core::vector3df(mIndexI+0.5, mGroundLevel+mHeight, mIndexJ+0.5) );
				core::vector3df diffVector=mWorld->getSoul()->getPosition()-Node->getPosition();
				Node->setRotation( core::vector3df(0.0, 180.0-atan2( diffVector.Z, diffVector.X )*180.0/core::PI, 0.0) );
      } else {
        fprintf( stdout, "couldn't allocate robot" );
      }
      entity = new cEntity( eRobot, Node, mWorld, mDevice );
			entity->setIndex( mIndexI, mIndexJ );
			entity->setHeight( 1.072501 );
			entity->setGroundLevel( mGroundLevel+mHeight );
			setNextEntity( entity );
			entity->setPreviousEntity( this );
    }
    break;
  case ePedestal:
    {
      scene::IAnimatedMeshSceneNode* Node=mDevice->getSceneManager()->addAnimatedMeshSceneNode( mWorld->getPedestalMesh(), mWorld->getNode() );
      if( Node ) {
				Node->setMaterialFlag( video::EMF_LIGHTING, false );
      	Node->setMaterialFlag( video::EMF_FOG_ENABLE, true );
				Node->setMaterialType( video::EMT_SOLID );
				Node->setPosition( core::vector3df(mIndexI+0.5, mGroundLevel+mHeight, mIndexJ+0.5) );
      } else {
        fprintf( stdout, "couldn't allocate pedestal" );
      }
      entity = new cEntity( ePedestal, Node, mWorld, mDevice );
			entity->setIndex( mIndexI, mIndexJ );
			entity->setHeight( sqrt(2.0) );
			entity->setGroundLevel( mGroundLevel+mHeight );
			setNextEntity( entity );
			entity->setPreviousEntity( this );
			scene::ITriangleSelector* entitySelector =
						mWorld->getSceneManager()->createTriangleSelector(mWorld->getPedestalMesh(), Node);
			Node->setTriangleSelector(entitySelector);
			entitySelector->drop();
    }
    break;
  case eSentinel:
    {
      scene::IAnimatedMeshSceneNode* Node=mDevice->getSceneManager()->addAnimatedMeshSceneNode(mWorld->getSentinelMesh(), mWorld->getNode());
      if(Node) {
				Node->setMaterialFlag( video::EMF_LIGHTING, false );
				Node->setPosition( core::vector3df(mIndexI+0.5, mGroundLevel+mHeight, mIndexJ+0.5) );
      } else {
        fprintf( stdout, "couldn't allocate Cerberus" );
      }

      cCerberus* cerberus = new cCerberus(eSentinel, Node, mWorld, mDevice);
			cerberus->setIndex( mIndexI, mIndexJ );
			cerberus->setHeight( sqrt(2.0) );
			cerberus->setGroundLevel( mGroundLevel+mHeight );
			cerberus->updateFieldOfView();
			entity=cerberus;
			setNextEntity( entity );
			entity->setPreviousEntity( this );
    }
    break;
  default:
    return NULL;
  }
	
	return entity;
}


int cEntityBase::getEnergy()
{
	int energy;
	
	switch( mType )
	{
	case eTree: energy = eTreeNrg; break;
	case eBoulder: energy = eBoulderNrg; break;
	case eRobot: energy = eRobotNrg; break;
	default: energy = 0; break;
	}
	
	return energy;
}


/**
 * Setting variables.
 */
cEntity::cEntity( eEntity type, scene::IAnimatedMeshSceneNode* Node, cWorld* world, IrrlichtDevice* device ) :
	cEntityBase( type, world, device ), mNode(Node)
{
	mVanishRotateSpeed = 720.0/1000.0;
	mVanishScaleSpeed = 0.002;
	mScale = 1.0;
}


/**
 * Remove node from scene manager
 */
cEntity::~cEntity()
{
	if( mNode )
		mNode->remove();
}


/**
 * Setting variables and initialie object.
 */
cElement::cElement( cWorld* world, IrrlichtDevice* device ) : cEntityBase( eElement, world, device )
{
	setIndex( 0, 0 );
  mLevel=0;
  mHeight = 0;
  mGroundLevel = 0.0;

 	mNode=NULL;
  mFlat=false;
  
  mLogger = mDevice->getLogger();
}


/**
 * Free entities on this element and remove node.
 */
cElement::~cElement()
{
	// remove stack of entities on this element
	cEntityBase* entity = getNextEntity();
	while( entity ) {
		cEntityBase* temp = entity->getNextEntity();
		delete entity;
		entity = temp;
	}
	setNextEntity( NULL );
	
	if( mNode )
		mNode->remove();
}


eStatus cEntity::processEvents(u32 time)
{
	switch(mStatus) {
			
		case eIdle:
			break;
			
			// Element is absorbed
		case eVanish:
		{
			core::vector3df rotation = mNode->getRotation();

			mScale -= mVanishScaleSpeed*(time-mLastProcessTime);
			if(mScale>0.01) {
				mNode->setScale(core::vector3df(mScale, mScale, mScale));
				
				// rotate entity
				rotation.Y += mVanishRotateSpeed*(time-mLastProcessTime);
				if(rotation.Y>=360.0)
					rotation.Y -= 360.0;				
				mNode->setRotation(rotation);
			} else
				mStatus = eDestroyed;				
		}
			break;
			
		default:
			break;
	}		
	
	mLastProcessTime = time;
			
	return mStatus;
}


/*!
 * Initialize soul and camera.
 *
 * @param camera : pointer to the camera node, of which the soul takes care of
 */
cSoul::cSoul( scene::ICameraSceneNode* camera ) : mCamera( camera ), mTheta( core::PI/2.0 ), 
        mThetaMin( 2.0f/180.0f*core::PI ), mThetaMax( 178.0f/180.0f*core::PI )
{
	// camera settings
  mRotateSpeed=1.0f;
  mCamera->setNearValue( 0.1f );
  mCamera->setFarValue( 12000.0f );
  
  // internal settings
  mEnergy = 37;
  mSeenFlag = false;
	
  // soul always looks to the middle of the landscape
  mPhi=atan2( (double)(16-mIndexJ), (double)(16-mIndexI) );
  
	// initialize View
  updateView();
}


/*!
 * Update view (direction where we looking at).
 */
void cSoul::updateView()
{
	mCamera->setPosition( core::vector3df( mIndexI+0.5, mGroundLevel+1.0, mIndexJ+0.5 ) );
	mCamera->setTarget( mCamera->getPosition() +
		core::vector3df(cos(mPhi)*sin(mTheta), cos(mTheta), sin(mPhi)*sin(mTheta)) );            
	mCamera->setUpVector( core::vector3df(-cos(mPhi)*cos(mTheta), sin(mTheta), -sin(mPhi)*cos(mTheta)) );            
}


/*!
 * 
 */
void cSoul::setTarget( core::vector3df target )
{
	core::vector3df diffVector=target-getPosition();

	mPhi=atan2( diffVector.Z, diffVector.X );
	mTheta=atan2( sqrt(diffVector.X*diffVector.X+diffVector.Z*diffVector.Z), diffVector.Y );
}


/*!
 * Change view (direction where we looking at).
 */
void cSoul::changeView(f32 px, f32 py)
{
	// determine new rotation angles
	mPhi -= px*mRotateSpeed;
	mTheta += py*mRotateSpeed;
	if( mTheta<mThetaMin ) mTheta=mThetaMin;
	if( mTheta>mThetaMax ) mTheta=mThetaMax;
	if( mPhi<0.0 ) 				mPhi+=2.0f*core::PI;
	if( mPhi>2*core::PI )  mPhi-=2.0f*core::PI;
	
	updateView();
}


/*!
 * Quick turn around.
 */
void cSoul::turnAround()
{
	// turn around 180 degrees
	mPhi += core::PI;
	if( mPhi>2*core::PI )  mPhi-=2.0f*core::PI;
	mTheta = core::PI/2;
	
	updateView();
}


/*!
 * Substract an amount of energy from the souls energy.
 * If the energy of the soul becomes 0 or lower return
 * false - the soul dies of exhaustion.
 *
 * @param sub : amount of energy to substract
 * @return false if energy exhausted, true otherwise
 */
bool cSoul::substractFromEnergy(int sub)
{ 
  mEnergy -= sub;

  if( mEnergy<=0 )
    return false;
  
  return true;
}


/**
 * Setting variables.
 */
cCerberus::cCerberus(eEntity type, scene::IAnimatedMeshSceneNode* node, cWorld* world, IrrlichtDevice* device) :
	cEntity(type, node, world, device)
{
	// allocate memory for arrays containg information what element is seen
	mWorldSize = mWorld->getSize();
  mView = new bool*[mWorldSize];
  for(int i=0; i<mWorldSize; i++) {
    mView[i] = new bool[mWorldSize];
		for(int j=0; j<mWorldSize; j++)
			mView[i][j] = false;
  }

	// set rotation
	mPhi=0; //floor(mWorld->getRandomNumber()*12.0)*30.0;
	mNode->setRotation(core::vector3df(0.0, mPhi, 0.0));
	updateFieldOfView();
	
	mWatchPeriod = 4000;
	mLastRotateTime = mDevice->getTimer()->getTime();
	printf("mLastRotateTime=%u\n", mLastRotateTime);
	mLastAbsorbTime = 0;
	mRecoverPeriod = 2000;
	
	// rotation speed synced to sound, + or -
	mRotationStep = 30.0; //* (mWorld->getRandomNumber()>0.5 ? 1.0 : -1.0 );
	mRotateSpeed = mRotationStep/880.0;
	mStatus = eWatch;
}


/*!
 * Free allocated memory.
 */
cCerberus::~cCerberus()
{
	// delete arrays
  for(int i=0; i<mWorldSize; i++)
    delete mView[i];
  delete[] mView;
}

/**
 * 
 */
eStatus cCerberus::processEvents(u32 time)
{
	//printf("Processing cerberus event %ud", time);
	switch(mStatus) {
	
	// Cerberus is watching the world until next rotation
	case eWatch:
		if(time-mLastAbsorbTime>mRecoverPeriod) {
			if(checkFieldOfView()) {
				mLastAbsorbTime = time;
			} else {
				if(time-mLastRotateTime>mWatchPeriod) {
					mStatus = eRotate;
					
					// play "moving chair" sound
					mWorld->getAudioDevice()->playSoundAtPosition(eCerberusTurns, mNode->getPosition());
				}
			}
		}
	
		break;
			
	// Cerberus rotates nicely synced to the sound
	case eRotate:
		{
			core::vector3df rotation = mNode->getRotation();
			
			// rotate cerberus
			rotation.Y += mRotateSpeed*(time-mLastProcessTime);
			if(rotation.Y>=360.0) {
				rotation.Y -= 360.0;
				mPhi -= 360.0;
			} else if(rotation.Y<0.0) {
				rotation.Y += 360.0;
				mPhi += 360.0;
			}				
			if(fabs(rotation.Y-mPhi)>=30.0) {  // finished rotation
				mPhi += mRotationStep;
				rotation.Y = mPhi;
				updateFieldOfView();
				mLastRotateTime = time;
				mStatus = eWatch;
			}
			mNode->setRotation(rotation);
		}
		break;

			// Element is absorbed
		case eVanish:
		{
			core::vector3df rotation = mNode->getRotation();
			
			mScale -= mVanishScaleSpeed*(time-mLastProcessTime);
			if(mScale>0.01) {
				mNode->setScale(core::vector3df(mScale, mScale, mScale));
				
				// rotate entity
				rotation.Y += mVanishRotateSpeed*(time-mLastProcessTime);
				if(rotation.Y>=360.0)
					rotation.Y -= 360.0;				
				mNode->setRotation(rotation);				
			} else
				mStatus = eDestroyed;				
		}
		break;

	default:
		break;
	}
	
	mLastProcessTime = time;	
	
	return mStatus;
}


bool cCerberus::checkFieldOfView()
{
	cEntityBase* entity = mWorld->checkForSources(mView);
	if(entity) {		
		cEntityBase* entityBelow = entity->getPreviousEntity();
		eEntity type = entity->getType();
		
		switch(type) {
			case eRobot:
				if(mWorld->getSoul()->getRobot()==entity) {
					mWorld->getSoul()->substractFromEnergy(1);
				} else {
					delete entity;
					entityBelow->setNextEntity(NULL);
					entityBelow->createEntity(eBoulder);
				}
				break;

			case eBoulder:	
				delete entity;
				entityBelow->setNextEntity(NULL);
				entityBelow->createEntity(eTree);
				break;

			case eTree:	
				delete entity;
				entityBelow->setNextEntity(NULL);
				break;

			default:
				break;
		}
		mWorld->createTree();
		return true;
	} else
		return false;
}


void cCerberus::updateFieldOfView()
{
	f32 phi;

	// determine which world element can be seen by the cerberus
	// TODO: it must be taken into consideration, that some elements
	//       are hidden
	for(int i=0; i<mWorldSize; i++) {
    for(int j=0; j<mWorldSize; j++) {
			phi = core::radToDeg(atan2((double)(-j+mIndexJ), (double)(i-mIndexI)));
			if(phi<0) phi=phi+360.0;
			if(phi>=360) phi=phi-360.0;
			mView[i][j] = fabs(phi-mPhi)<=15.0 || fabs(phi-mPhi)>345.0;
		}
	}

	if(1) {
		printf("----------------------------------------------- %d\n", mWorldSize);
		for(int j=mWorldSize-1; j>=0; j--) {
			printf("%2d: ", j);
			for(int i=0; i<mWorldSize; i++) {
				if(mWorld->getSoul()->getIndexI()==i && mWorld->getSoul()->getIndexJ()==j)
					printf("R");
				else
					printf("%c", mView[i][j] ? '1' : '0');
			}
			printf("\n");
		}
	}
}