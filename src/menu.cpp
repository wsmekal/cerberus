
#include "menu.h"
#include "cerberus.h"
#include "landscape.h"

cMenuScreen::cMenuScreen( IrrlichtDevice* device, cConfiguration* configuration, cAudioDevice* audioDevice ) :
    mDevice( device ), mConfiguration( configuration ), mAudioDevice( audioDevice )
{    
  // set pointers to basic irrlicht objects
	mDriver = mDevice->getVideoDriver();
	mSmgr = mDevice->getSceneManager();
	mEnv = mDevice->getGUIEnvironment();
	mLogger = mDevice->getLogger();  
  
  // load icons/logos/textures
	mDriver->setTextureCreationFlag( video::ETCF_ALWAYS_32_BIT, true );
  tIrrlichtLogo = mDriver->getTexture( "media/irrlichtlogo2.png" );
  
	// add irrlicht logo
	mEnv->addImage( tIrrlichtLogo, core::position2d<s32>(10, mConfiguration->height-92) );

	// catch and disable mouse cursor
  mDevice->getCursorControl()->setPosition( 0.5f, 0.5f );
	mDevice->getCursorControl()->setVisible( false );

  // create world
  mWorld = new cWorld( mDevice, mAudioDevice, 1, 33, 15, 4, 0.9 );
  mWorld->create();
  if( mWorld->getNode() ) {
    mWorld->getNode()->setPosition( core::vector3df(0, 0, 0) );
    mWorld->getNode()->setRotation( core::vector3df(0, 0, 0) );
  }

  // show alien sky
	scene::ISceneNode* skynode=mSmgr->addSkyDomeSceneNode( mDriver->getTexture("media/alien_world.jpg"), 16, 16, 1.0, 2.0 );
	skynode->setRotation( core::vector3df(25, 100, 0) );

	// attach event receiver
	mDevice->setEventReceiver( this );

  // Game starts with a view of the world
  mWorld->showStartScreen();

	// the menu starts with the start/welcome screen
  mMenuStatus=eMenuStart;
  
  // play menu music
  mAudioDevice->playMusic( "music/NIN-Ghosts_I-3.ogg" );
}


cMenuScreen::~cMenuScreen( void )
{
	// delete allocated objects
	if( mWorld )
		delete mWorld;

	// remove event receiver
	mDevice->setEventReceiver( NULL );
	
	// clear SceneManager
	mSmgr->clear();
}



bool cMenuScreen::OnEvent( const SEvent& event )
{
  switch( mMenuStatus ) {
  case eMenuStart:
	  // process keys events
	  if( event.EventType == EET_KEY_INPUT_EVENT && !event.KeyInput.PressedDown ) {
			switch (event.KeyInput.Key) {
			case KEY_ESCAPE: // quit game
				mMenuStatus=eMenuQuit;
				return true;
			case KEY_KEY_S: // take and save screenshot
				mDriver->writeImageToFile( mDriver->createScreenShot(), "screenshot.png" );
				return true;
			default:
				mMenuStatus=eMenuEnterLandscape;
				return true;
			}
	  }
		break;
  case eMenuEnterLandscape:
	  // process keys events
	  if( event.EventType == EET_KEY_INPUT_EVENT && !event.KeyInput.PressedDown ) {
			switch (event.KeyInput.Key) {
			case KEY_ESCAPE: // quit game
        mLandscapeNumber="";
        mLandscapeCode="";
				mMenuStatus=eMenuStart;
				return true;
			case KEY_KEY_S: // take and save screenshot
				mDriver->writeImageToFile( mDriver->createScreenShot(), "screenshot.png" );
				return true;
			case KEY_KEY_0: case KEY_KEY_1: case KEY_KEY_2: case KEY_KEY_3: case KEY_KEY_4:
			case KEY_KEY_5: case KEY_KEY_6: case KEY_KEY_7: case KEY_KEY_8: case KEY_KEY_9:
				if( mLandscapeNumber.size()<4 )
					mLandscapeNumber.append( (char)(event.KeyInput.Key-KEY_KEY_0)+'0' );
				return true;
			case KEY_BACK:
				if( mLandscapeNumber.size()>0 )
					mLandscapeNumber.erase( mLandscapeNumber.size()-1 );
				return true;
			case KEY_RETURN:
				if( atoi(mLandscapeNumber.c_str())==1 || mLandscapeNumber.size()==0 ) {
				  mLandscapeNumber="1";
					mMenuStatus=eMenuProceed;
				}	else if( atoi(mLandscapeNumber.c_str())!=0 )
          mMenuStatus=eMenuEnterCode;
				return true;			
			default:
				return true;
			}
	  }
		break;
  case eMenuEnterCode:	
	  // process keys events
	  if( event.EventType == EET_KEY_INPUT_EVENT && !event.KeyInput.PressedDown ) {
			switch (event.KeyInput.Key) {
			case KEY_ESCAPE: // quit game
        mLandscapeNumber="";
        mLandscapeCode="";
				mMenuStatus=eMenuStart;
				return true;
			case KEY_KEY_S: // take and save screenshot
				mDriver->writeImageToFile( mDriver->createScreenShot(), "screenshot.png" );
				return true;
			case KEY_KEY_0: case KEY_KEY_1: case KEY_KEY_2: case KEY_KEY_3: case KEY_KEY_4:
			case KEY_KEY_5: case KEY_KEY_6: case KEY_KEY_7: case KEY_KEY_8: case KEY_KEY_9:
				if( mLandscapeCode.size()<8 )
					mLandscapeCode.append( (char)(event.KeyInput.Key-KEY_KEY_0)+'0' );
				return true;
			case KEY_BACK:
				if( mLandscapeCode.size()>0 )
					mLandscapeCode.erase( mLandscapeCode.size()-1 );
				return true;
			case KEY_RETURN:
        if( cWorld::getLandscapeCode(atoi(mLandscapeNumber.c_str()))==atoi(mLandscapeCode.c_str()) )
					mMenuStatus=eMenuProceed;
        else {
					fprintf( stdout, "level: %s\ncorrect code: %08d\nyour code: %08d\n", mLandscapeNumber.c_str(),
									   atoi(mLandscapeCode.c_str()), cWorld::getLandscapeCode(atoi(mLandscapeNumber.c_str())) );
          mLandscapeNumber="";
          mLandscapeCode="";
					mMenuStatus=eMenuStart;
        }
				return true;			
			default:
				return true;
			}
	  }
		break;
	default:
		break;
	}

  return false;
}

/*!
 * Menu, ask for landscape number and code
 */
bool cMenuScreen::run( unsigned int* level )
{
	int lastFPS = -1, fps;
  double oldtime=0, period=0, timeset=0;
  core::stringw title = L"Cerberus [";
  title += mDriver->getName();
  title += "] FPS: ";
  core::stringw fulltitle;

	gui::IGUIFont* menuFont=mEnv->getFont( "media/zeroes.xml" );
	
  core::stringw anyKeyString=L"PRESS ANY KEY";
  core::stringw enterLandscapeString=L"ENTER LANDSCAPE";
  core::stringw enterCodeString=L"ENTER CODE";

	// clear input strings
	mLandscapeNumber="";
	mLandscapeCode="";

  timeset=(double)(mDevice->getTimer()->getRealTime());
	while( mDevice->run() && mMenuStatus!=eMenuQuit && mMenuStatus!=eMenuProceed ) {
    if( mDevice->isWindowActive() ) {      
      // limit output to max_fps
      period = mConfiguration->fps_period-((double)(mDevice->getTimer()->getRealTime())-oldtime);
      if( period>0 )
        mDevice->sleep( (u32)period, false );
      oldtime=(double)(mDevice->getTimer()->getRealTime());
      
      switch( mMenuStatus ) {
      case( eMenuStart ):
        mDriver->beginScene( true, true, video::SColor(255, 0, 0, 0) );
        mSmgr->drawAll();
        menuFont->draw( anyKeyString.c_str(), core::rect<s32>(mConfiguration->width/2, mConfiguration->height-50,
				                                                      mConfiguration->width/2, mConfiguration->height-50),
												video::SColor(255,255,255,255), true, false, NULL );
        mDriver->endScene();
        break;
      case( eMenuEnterLandscape ):
        // draw everything
        mDriver->beginScene( true, true, video::SColor(255, 0, 0, 0) );
        mSmgr->drawAll();
        menuFont->draw( enterLandscapeString.c_str(), core::rect<s32>(mConfiguration->width/5, mConfiguration->height-100,
																																			mConfiguration->width/5, mConfiguration->height-100),
												video::SColor(255,255,255,255), false, false, NULL );
        menuFont->draw( core::stringw(mLandscapeNumber.c_str()).c_str(),
												core::rect<s32>(mConfiguration->width/5, mConfiguration->height-60,
																				mConfiguration->width/5, mConfiguration->height-60),
												video::SColor(255,255,255,255), false, false, NULL );
        mDriver->endScene();
        break;
      case( eMenuEnterCode ):
        // draw everything
        mDriver->beginScene( true, true, video::SColor(255, 0, 0, 0) );
        mSmgr->drawAll();
        menuFont->draw( enterCodeString.c_str(), core::rect<s32>(mConfiguration->width/5, mConfiguration->height-100,
														  																	 mConfiguration->width/5, mConfiguration->height-100),
												video::SColor(255,255,255,255), false, false, NULL );
        menuFont->draw( core::stringw(mLandscapeCode.c_str()).c_str(),
												core::rect<s32>(mConfiguration->width/5, mConfiguration->height-60,
																				mConfiguration->width/5, mConfiguration->height-60),
												video::SColor(255,255,255,255), false, false, NULL );
        mDriver->endScene();
        break;
      case( eMenuQuit ):
			case( eMenuProceed ):
				// the loop will drop out before, so we do nothing here
				break;
      }

      // display frames per second in window title
      fps = mDriver->getFPS();
      if( lastFPS!=fps ) {
        fulltitle = title;
        fulltitle += fps;

        mDevice->setWindowCaption( fulltitle.c_str() );
        lastFPS = fps;
      }
      
    }
  }
	
	// return to main app
	if( mMenuStatus==eMenuQuit )
		return false;
	else {
		*level=(unsigned int)atoi( mLandscapeNumber.c_str() );
		return true;
	}
}
