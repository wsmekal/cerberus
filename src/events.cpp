
// cerberus headers
#include "cerberus.h"


/**
 * Constructor mainly initializes variables
 */
cEventManager::cEventManager() : mFirstEvent( NULL ), mLastEvent( NULL )
{
}


/**
 * Deconstructor deletes only events which are dropped.
 */
cEventManager::~cEventManager()
{
  cEventBase* temp;
  cEventBase* event = mFirstEvent;
  
  // delete dopped events
  while( event ) {
  	temp = event->getNextEvent();
  	if( event->isDropped() )
  		delete event;
  	event = temp;
  }
}

/**
 * Add an event to the list.
 *
 * @param event : address of event to be added
 */
void cEventManager::registerEvent( cEventBase* event )
{
	if( !mFirstEvent ) {
		mFirstEvent = event;
		mLastEvent = event;
	} else {
		mLastEvent->setNextEvent( event );
		event->setPreviousEvent( mLastEvent );
		mLastEvent = event;
	}
}


/**
 * Removed an event from the list. If the event given by
 * it's address is not in the list, nothing happens. Event is
 * deleted if dropped.
 *
 * @param event : address of event to be removed
 */
void cEventManager::removeEvent( cEventBase* event )
{
	cEventBase* listEvent = mFirstEvent;
	
	while( listEvent ) {
		if( listEvent==event ) {
			// remove event from linked list
			if( event->getPreviousEvent() )
				event->getPreviousEvent()->setNextEvent( event->getNextEvent() );
			if( event->getNextEvent() )
				event->getNextEvent()->setPreviousEvent( event->getPreviousEvent() );
				
			// delete event if dropped
			if( event->isDropped() )
				delete event;
			break;
		}
	}
}


/**
 * All events in the list are processed. The time
 * parameter is passed to the events, which are all
 * mainly time based.
 *
 * @param time : Virtual time of device.
 */
void cEventManager::process( u32 time )
{
  cEventBase* event = mFirstEvent;
  
  // process all events
  while( event ) {
  	event->process( time );
  	event = event->getNextEvent();
  }
}
