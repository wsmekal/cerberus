

#ifndef __MENU_H
#define __MENU_H

// include irrlicht headers
#include "irrlicht.h"
using namespace irr;

// forward declarations
class cWorld;
class mt19937;
class cConfiguration;
class cAudioDevice;

enum eMenuStatus { eMenuStart=1, eMenuEnterLandscape, eMenuEnterCode, eMenuProceed, eMenuQuit };

class cMenuScreen : public IEventReceiver
{
public:
  // Constructor and deconstructor
	cMenuScreen( IrrlichtDevice* device, cConfiguration* configuration, cAudioDevice* audioDevice );
	~cMenuScreen();

  // run the menu
	bool run( unsigned int* level );

private:
  // Event handler
	bool OnEvent( const SEvent& event );

private:
	IrrlichtDevice* mDevice;
  video::IVideoDriver* mDriver;
	scene::ISceneManager* mSmgr;
	gui::IGUIEnvironment* mEnv;
  ILogger *mLogger;

  // pointer to landscape
  cWorld* mWorld;

  // pointer to configuration object
  cConfiguration* mConfiguration;
  
  // pointer to Audio device
  cAudioDevice* mAudioDevice;

  // status of the menu
  eMenuStatus mMenuStatus;

  // pointers to textures
  video::ITexture* tIrrlichtLogo;    //!< Irrlich Logo
	
	// input strings
	core::stringc mLandscapeNumber;
	core::stringc mLandscapeCode;
};


#endif // __MENU_H
