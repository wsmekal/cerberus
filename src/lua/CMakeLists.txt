
################################################################
# lua and lualib library version 5.0
################################################################
set(lua_LIB_SRC
	lapi.c lcode.c ldebug.c ldo.c ldump.c lfunc.c	lgc.c	llex.c
  lmem.c lobject.c lopcodes.c lparser.c lstate.c lstring.c
	ltable.c ltests.c	ltm.c lundump.c lvm.c lzio.c
)
include_directories(.)
add_library(lua STATIC ${lua_LIB_SRC})

set(lualib_LIB_SRC
	lib/lauxlib.c lib/lbaselib.c lib/ldblib.c lib/liolib.c
	lib/lmathlib.c lib/loadlib.c lib/lstrlib.c lib/ltablib.c
)
add_library(lualib STATIC ${lualib_LIB_SRC})


