
// include irrlicht headers
#include "irrlicht.h"
using namespace irr;

#include "cerberus.h"
#include "landscape.h"
#include "entities.h"

// Mersenne Twister RNG
#include "mt19937.hpp"


class cDummyEvent : public cEventBase
{
public:
	cDummyEvent(cWorld* world) :
		mWorld( world )
	{
	}
	
	void process( u32 time )
	{
	}

private:
	cWorld* mWorld;
};

/*    // Add rotating event to event manager
    cEventBase* event = new cTurnCerberusEvent( this, cerberus, mDevice->getTimer()->getTime(), 2000 );
    mEventManager->registerEvent( event );
    event->drop();
 */


/*!
 * 
 */
cWorld::cWorld( IrrlichtDevice* device, cAudioDevice* audioDevice, int level, int size,
                int energy, double scalarStart, double hScalar ) :
    mDevice( device ), mAudioDevice( audioDevice ), mLevel( level ), mSize( size ),
    mEnergy( energy ), mScalarStart( scalarStart ), mHScalar( hScalar )
{
	mSoul=NULL;

	// set landscape name (e.g. "LANDSCAPE 0545")
	mName.append( L"LANDSCAPE " );
	char buffer[5];
	snprintf( buffer, 5, "%04d", mLevel );
	mName.append( buffer );

  // set pointers to basic irrlicht objects
	mDriver = mDevice->getVideoDriver();
	mSmgr = mDevice->getSceneManager();
	mLogger = mDevice->getLogger();  
	
	// create event manager;
	mEventManager = new cEventManager;

  // preload meshes
  mTreeMesh     = mSmgr->getMesh( "media/tree.obj" );
  mBoulderMesh  = mSmgr->getMesh( "media/boulder.obj" );
  mRobotMesh    = mSmgr->getMesh( "media/robot.obj" );  
  mPedestalMesh = mSmgr->getMesh( "media/pedestal.obj" );  
  mSentinelMesh = mSmgr->getMesh( "media/sentinel.obj" );  

  // start RNG
  rng = new mt19937( ((unsigned long)(mLevel)+8749UL)*23095UL );

  // allocate memory for arrays containg world information
  mPoints = new double*[mSize+1];
  for( int i=0; i<mSize+1; i++ ) {
    mPoints[i] = new double[mSize+1];
    memset( mPoints[i], 0, sizeof(double)*(mSize+1) );
  }

  // allocate memory for the floor elements
  mElements = new cElement**[mSize];
  for( int i=0; i<mSize; i++ ) {
    mElements[i] = new cElement*[mSize];
    for( int j=0; j<mSize; j++ )
      mElements[i][j] = new cElement(this, mDevice);
  }

  init();
}


cWorld::~cWorld()
{
  // delete arrays
  for( int i=0; i<mSize+1; i++ )
    delete mPoints[i];
  delete mPoints;
	
  for( int i=0; i<mSize; i++ ) {
    for( int j=0; j<mSize; j++ )
      delete mElements[i][j];
		delete mElements[i];
	}
	delete mElements;

	// delete soul
	if( mSoul )
		delete mSoul;
  
  // remove RNG
  delete rng;
	
	// remove node
	mCamera->remove();
	mNode->remove();
	
	// remove event manager
	delete mEventManager;
}


double cWorld::gaussian( double x0, double sigma )
{
  double x;
  
  for(;;) {
    x = x0 + 4.0*sigma*(-1.0 + getRandomNumber()*2.0);
    if( getRandomNumber()<exp(-0.5*pow((x-x0)/sigma, 2.0)) )
      break;
  }
  
  return x;
}


double cWorld::gaussian_4( double sigma, double a1, double a2, double a3, double a4 )
{
  return gaussian( (a1+a2+a3+a4 )/4.0, sigma );
}

double cWorld::gaussian_3( double sigma, double a1, double a2, double a3 )
{
  return gaussian( (a1+a2+a3 )/3.0, sigma );
}

void cWorld::init()
{
  double delta = mScalarStart;
  int N = mSize;
  double max, min;
      
  mPoints[0][0] = gaussian( 0, delta );
  mPoints[N][0] = gaussian( 0, delta );
  mPoints[0][N] = gaussian( 0, delta );
  mPoints[N][N] = gaussian( 0, delta );

  int d = N / 2;
  int D = N;

  while (d >= 1)
  {
#ifdef DEBUG
    std::cout <<  "doing: d=" << d << " D=" << D << " delta=" << delta << "\n";
#endif
    delta = delta*sqrt(0.5*mHScalar);

#ifdef DEBUG
    std::cout << "middlepoints!\n";
#endif
    // middle points (stage II)
    for( int x=d; x<=N-d; x=x+D )
      for( int y=d; y<=N-d; y=y+D )
        mPoints[x][y] = gaussian_4( delta, mPoints[x - d][y - d], mPoints[x + d][x - d],
                                  mPoints[x - d][y + d], mPoints[x + d][y + d]);
    
    // then do the stage I things
    delta = delta * sqrt(0.5 * mHScalar);
#ifdef DEBUG
    std::cout << "borders!\n";
#endif
    // do the borders
    for ( int x=d; x<=N-d; x=x+D ) {
       mPoints[x][0] = gaussian_3(delta, mPoints[x + d][0], mPoints[x - d][0], mPoints[x][d]);
       mPoints[x][N] = gaussian_3(delta, mPoints[x + d][N], mPoints[x - d][N], mPoints[x][N - d]);
       mPoints[0][x] = gaussian_3(delta, mPoints[0][x + d], mPoints[0][x - d], mPoints[d][x]);
       mPoints[N][x] = gaussian_3(delta, mPoints[N][x + d], mPoints[N][x - d], mPoints[N - d][x]);
    }
    
#ifdef DEBUG
    std::cout << "interiors!\n";
#endif
    for ( int x=d; x<=N-d; x=x+D )
       for (int y=D; y<=N-d; y=y+D )
          mPoints[x][y] = gaussian_4(delta,mPoints[x+d][y],mPoints[x-d][y],mPoints[x][y+d],mPoints[x][y-d]);

#ifdef DEBUG
    std::cout << "interiors2!\n";
#endif
    for ( int x = D; x <= N-d; x = x + D)
       for (int y = d; y <= N-d; y = y + D)
          mPoints[x][y] = gaussian_4(delta,mPoints[x+d][y],mPoints[x-d][y],mPoints[x][y+d],mPoints[x][y-d]);
    if (d == 1)
      break;
    d = d / 2;
    D = D / 2;
  }

  min=1e300; max=-1e300;
  for( int x=0; x<mSize+1; x++ ) {
    for (int y = 0; y < mSize+1; y ++) {
      mPoints[x][y] = round(mPoints[x][y]);
       if( mPoints[x][y]<min )
         min=mPoints[x][y];
       if( mPoints[x][y]>max )
         max=mPoints[x][y];
    }
  }

  for( int x=0; x<mSize+1; x++ ) {
    for (int y = 0; y < mSize+1; y ++) {
      mPoints[x][y] -= min;
    }
  }

  /* remove single spikes */
  for( int x=1; x<mSize; x++ ) {
    for (int y=1; y<mSize; y ++) {
      double k = mPoints[x][y+1];
      if( (mPoints[x-1][y]==k) && (mPoints[x][y-1]==k) && (mPoints[x+1][y]==k) )
        mPoints[x][y] = k;
    }
  }
}


void cWorld::print()
{
  // then print the points
  for( int x = 0; x < mSize+1; x++ ) {
    for( int y = 0; y < mSize+1; y++ )
       std::cout << x << " " << y << " " << mPoints[x][y] << "   " << "\n";
    std::cout << "\n";
  }
}

void cWorld::create( void )
{
  double y1, y2, y3;
  int maxHeight=0;

	// add parent world managment node
  mNode = mSmgr->addEmptySceneNode();

  for( int i=0; i<mSize; i++ ) {
    for( int j=0; j<mSize; j++ ) {
      video::SColor color;
      
			// element buffer consists of 2*2 triangles (=12 Vertices), front and backside
      scene::SMeshBuffer* elementBuffer = new scene::SMeshBuffer();
      elementBuffer->Indices.set_used( 12 );
      elementBuffer->Vertices.set_used( 12 );
      for( int k=0; k<12; ++k )
        elementBuffer->Indices[k] = k;
  
      // determine color of element
      double height = mPoints[i][j];
      if( (mPoints[i][j+1]!=height) || (mPoints[i+1][j+1]!=height) || (mPoints[i+1][j]!=height) ) {
        // if the element IS NOT parallel to the xy plane the element
        // is either grey or black
        if( (i+j)%2 == 0 )
          color.set( 255, 100, 100, 100 );
        else
          color.set( 255, 0, 0, 0 );
        
        // set land information
        
        mElements[i][j]->setFlat( false );
      } else {
        // if the element IS parallel to the xy plane the element
        // is either blue (tuerkis) or green
        if( (i+j)%2 == 0 )
          color.set( 255, 0, 204, 0 );
        else
          color.set( 255, 0, 128, 182 );

        // set land information
        mElements[i][j]->setFlat( true );
        
        maxHeight = height > maxHeight ? height : maxHeight;
      }
      
      y1=mPoints[i][j+1]-mPoints[i][j];
      y2=mPoints[i+1][j+1]-mPoints[i][j];
      y3=mPoints[i+1][j]-mPoints[i][j];
      y1*=sqrt(2); y2*=sqrt(2); y3*=sqrt(2);
      if( fabs(y2)>=fabs(y3-y1) ) {
        elementBuffer->Vertices[0]  = video::S3DVertex( 0, 0,0,  y1-y2,1,-y1, color, 1,0 );
        elementBuffer->Vertices[1]  = video::S3DVertex( 0,y1,1,  y1-y2,1,-y1, color, 1,0 );
        elementBuffer->Vertices[2]  = video::S3DVertex( 1,y2,1,  y1-y2,1,-y1, color, 1,0 );
				//| 0| | 1|  |y1-y2|
				//|y1|x|y2|= |    1|
				//| 1| | 1|  |  -y1|
        
        elementBuffer->Vertices[3]  = video::S3DVertex( 0, 0,0,  -y3,1,y3-y2, color, 1,0 );
        elementBuffer->Vertices[4]  = video::S3DVertex( 1,y2,1,  -y3,1,y3-y2, color, 1,0 );
        elementBuffer->Vertices[5]  = video::S3DVertex( 1,y3,0,  -y3,1,y3-y2, color, 1,0 );
				//| 1| | 1|  |  -y3|
				//|y2|x|y3|= |    1|
				//| 1| | 0|  |y3-y2|

        elementBuffer->Vertices[6]  = video::S3DVertex( 0, 0,0,  -y1+y2,-1,y1, video::SColor(255,0,0,0), 1,0 );
        elementBuffer->Vertices[7]  = video::S3DVertex( 1,y2,1,  -y1+y2,-1,y1, video::SColor(255,0,0,0), 1,0 );
        elementBuffer->Vertices[8]  = video::S3DVertex( 0,y1,1,  -y1+y2,-1,y1, video::SColor(255,0,0,0), 1,0 );
				//| 1| | 0|  |y2-y1|
				//|y2|x|y1|= |   -1|
				//| 1| | 1|  |   y1|
        
        elementBuffer->Vertices[9]   = video::S3DVertex( 0, 0,0,  y3,-1,-y3+y2, video::SColor(255,0,0,0), 1,0 );
        elementBuffer->Vertices[10]  = video::S3DVertex( 1,y3,0,  y3,-1,-y3+y2, video::SColor(255,0,0,0), 1,0 );
        elementBuffer->Vertices[11]  = video::S3DVertex( 1,y2,1,  y3,-1,-y3+y2, video::SColor(255,0,0,0), 1,0 );
				//| 1| | 1|  |  -y3|
				//|y3|x|y2|= |    1|
				//| 0| | 1|  |y3-y2|
      } else {
        elementBuffer->Vertices[0]  = video::S3DVertex( 0, 0,0,  -y3,1,-y1, color, 1,0 );
        elementBuffer->Vertices[1]  = video::S3DVertex( 0,y1,1,  -y3,1,-y1, color, 1,0 );
        elementBuffer->Vertices[2]  = video::S3DVertex( 1,y3,0,  -y3,1,-y1, color, 1,0 );
				//| 0| | 1| |-y3|
				//|y1|x|y3|=|  1|
				//| 1| | 0| |-y1|

        elementBuffer->Vertices[3]  = video::S3DVertex( 1,y3,0,  y1-y2,1,y3-y2, color, 1,0 );
        elementBuffer->Vertices[4]  = video::S3DVertex( 0,y1,1,  y1-y2,1,y3-y2, color, 1,0 );
        elementBuffer->Vertices[5]  = video::S3DVertex( 1,y2,1,  y1-y2,1,y3-y2, color, 1,0 );
				//|   -1| |    0| |y1-y2|
				//|y1-y3|x|y2-y3|=|    1|
				//|    1| |    1| |y3-y2|

        elementBuffer->Vertices[6]  = video::S3DVertex( 0, 0,0,  y3,-1,y1, video::SColor(255,0,0,0), 1,0 );
        elementBuffer->Vertices[7]  = video::S3DVertex( 1,y3,0,  y3,-1,y1, video::SColor(255,0,0,0), 1,0 );
        elementBuffer->Vertices[8]  = video::S3DVertex( 0,y1,1,  y3,-1,y1, video::SColor(255,0,0,0), 1,0 );
				//| 1| | 0| |y3|
				//|y3|x|y1|=|-1|
				//| 0| | 1| |y1|

        elementBuffer->Vertices[9]   = video::S3DVertex( 1,y3,0,  -y1+y2,-1,-y3+y2, video::SColor(255,0,0,0), 1,0 );
        elementBuffer->Vertices[10]  = video::S3DVertex( 1,y2,1,  -y1+y2,-1,-y3+y2, video::SColor(255,0,0,0), 1,0 );
        elementBuffer->Vertices[11]  = video::S3DVertex( 0,y1,1,  -y1+y2,-1,-y3+y2, video::SColor(255,0,0,0), 1,0 );
				//|    0| |   -1| |y2-y1|
				//|y2-y3|x|y1-y3|=|   -1|
				//|    1| |    1| |y2-y3|
      }
      elementBuffer->recalculateBoundingBox();
      
      scene::SMesh* elementMesh = new scene::SMesh();
      elementMesh->addMeshBuffer( elementBuffer );
			elementBuffer->drop(); // we don't need the buffer anymore

      scene::IMeshSceneNode* elementNode = mSmgr->addMeshSceneNode( elementMesh, mNode );
      if( !elementNode ) {
        mLogger->log( "Couldn't create element node!", ELL_ERROR );
        exit( -1 );
      }
			elementMesh->drop();  // we don't need the mesh anymore
			
      elementNode->setPosition( core::vector3df(i, mPoints[i][j]*sqrt(2), j) );
      
      elementNode->setMaterialFlag( video::EMF_LIGHTING, false );
      elementNode->setMaterialFlag( video::EMF_FOG_ENABLE, true );
      elementNode->setMaterialType( video::EMT_SOLID );
      elementNode->getMaterial(0).AmbientColor = color;
      elementNode->getMaterial(0).DiffuseColor = color;
      elementNode->getMaterial(0).EmissiveColor = color;
      
      scene::ITriangleSelector* elementSelector = mSmgr->createTriangleSelector( elementMesh,
                                                    elementNode );
      elementNode->setTriangleSelector( elementSelector );
      elementSelector->drop();

      mElements[i][j]->setNode( elementNode );
			mElements[i][j]->setIndex( i, j );
      mElements[i][j]->setLevel( (int)(mPoints[i][j]) );
    }
  }
  
	// add energy as trees to world
  int curr_energy=0;
  while(curr_energy<mEnergy) {
		createTree();
    curr_energy+=1;
  }

 	// Add camera, which is controlled by our event receiver. The provided
  // cameras (FPS, Maya) were not usable for this program (or I didn't understand
  // how they should work!?)
  mCamera = mSmgr->addCameraSceneNode( mNode );
  mSoul = new cSoul( mCamera );

  // find suitable spot for soul
  bool found=false;
  while( !found ) {
    int i, j;
    
    i=(int)(getRandomNumber()*mSize);
    j=(int)(getRandomNumber()*mSize);
    
    if( !mElements[i][j]->createEntity( eRobot ) )
      continue;
    else
    	found=true;
    
    mElements[i][j]->getNextEntity()->getNode()->setVisible( false );
    mSoul->setRobot( mElements[i][j]->getNextEntity() );
    mSoul->setIndex( i, j );
    mSoul->setGroundLevel( mElements[i][j]->getGroundLevel() );
  }
  
  // find suitable spot for sentinel
  found=false;
  while( !found ) {
    int i, j;
    
    i=(int)(getRandomNumber()*mSize);
    j=(int)(getRandomNumber()*mSize);
    
    if( mElements[i][j]->getLevel()!=maxHeight || !mElements[i][j]->isFlat() )
      continue;
    else
    	found=true;
    
    printf("create pedestal @ %d %d\n", i, j); 
    
    cEntityBase* pedestal = mElements[i][j]->createEntity( ePedestal );
    //cEntityBase* cerberus = 
    pedestal->createEntity(eSentinel);
  }
	mDefeated=false;
}

void cWorld::createTree()
{
	int i, j;
	
	do {
		i=(int)(getRandomNumber()*mSize);
		j=(int)(getRandomNumber()*mSize);
	} while(!(mElements[i][j]->createEntity(eTree)));
}

/**
 * Find the entity (element, robot, boulder, ...) on which the
 * player points at.
 *
 * return A pointer to the entity which is selected
 */
cEntityBase* cWorld::findEntity()
{
	core::line3d<f32> line;
	line.start = mCamera->getPosition();
	line.end = line.start + (mCamera->getTarget() - line.start).normalize() * 1000.0f;

	cEntityBase* entity;
	cEntityBase* entitySave=NULL;
	core::vector3df intersection;
	core::triangle3df tri;
	const scene::ISceneNode* outNode;
  f32 shortestDistance=1.0e30;
	bool good_triangle=false;
  
  for(int i=0; i<mSize; i++)
    for(int j=0; j<mSize; j++) {
    	entity = mElements[i][j];
    	while(entity) {
				scene::ITriangleSelector* selector=entity->getNode()->getTriangleSelector();
				if(mSmgr->getSceneCollisionManager()->getCollisionPoint(line, selector, intersection, tri, outNode)) {
					if(entity->getType()==ePedestal) {
						if(tri.getNormal().X==0 && tri.getNormal().Z==0) 
							good_triangle=true;
						else
							good_triangle=false;
					}
					if(mCamera->getPosition().getDistanceFrom(intersection)<shortestDistance) {
						entitySave = entity;
						shortestDistance = mCamera->getPosition().getDistanceFrom(intersection);
					}
				}
				entity = entity->getNextEntity();
			}
    }
	
	if(entitySave) {
		if(entitySave->getType()==ePedestal && !good_triangle)
			return NULL;
	}
	
	return entitySave;
}


bool cWorld::createEntity(eEntity entityType, cEntityBase* entityBelow)
{
	assert(entityBelow);

	if( entityBelow->createEntity(entityType) ) {
	  switch( entityType ) {
  	case eTree:
  		mSoul->substractFromEnergy( 1 );
  		break;
  	case eBoulder:
  		mSoul->substractFromEnergy( 2 );
  		break;
  	case eRobot:
  		mSoul->substractFromEnergy( 3 );
  		break;
  	default:
  		break;
		}
		
		return true;
	} else
		return false;
}


/*!
 * If the player clicks the left mouse button or the 'a' key
 * a entity will be absorbed. For this the player must point to a
 * a boulder or element below the entity to be absorbed. A boulder
 * can also be absorbed by just pointing to it (if there is nothing
 * above). The absorbed energy will be added to the players energy
 * account.
 *
 * @param entityBelow : entity below the entity to be absorbed or free
 *                      boulder
 * @return the amount of energy absorbed.
 */
int cWorld::absorbEntity(cEntityBase* entityBelow)
{
	assert(entityBelow);
	
	// if world is defeated, no absorbing allowed any more
	if(mDefeated)
		return 0;

  // only entities above an element or boulder may be absorbed
  if((entityBelow->getType() != eElement) && (entityBelow->getType() != eBoulder) && (entityBelow->getType() != ePedestal))
  	return 0;

	// is there something to absorb above?
	int energy=0;
  if(entityBelow->getNextEntity()) {
		// there must be nothing above the next element
		if(entityBelow->getNextEntity()->getNextEntity())
			return 0;

		// remember energy and delete entity  
		if(entityBelow->getNextEntity()->getStatus()!=eVanish && entityBelow->getNextEntity()->getStatus()!=eDestroyed) {
			entityBelow->getNextEntity()->setStatus(eVanish);
			energy = entityBelow->getNextEntity()->getEnergy();
		}

	} else {
		// if there is nothing above we absorb entityBelow if
		// it is an boulder
		if(entityBelow->getType() == eBoulder && entityBelow->getStatus()==eIdle) {
			entityBelow->setStatus(eVanish);
			energy = entityBelow->getEnergy();
		}
	}
	
	mSoul->addToEnergy(energy);
	
	return energy;
}


/*!
 * If the player wants to transfer his soul to another
 * robot this function checks if this is possible and
 * transfers the soul.
 *
 * @param entityBelow : entity below the robot
 * @return true if there was an transfer, false otherwise
 */
bool cWorld::transferSoul( cEntityBase* entityBelow )
{
	assert(entityBelow);

  // there must be a robot above the found entity
  cEntityBase* robot = entityBelow->getNextEntity();
  if( !robot )
  	return false;
  else {
  	if( robot->getType() != eRobot )
  		return false;
  }
  
  // do the actual transfer
  return doTransferSoul( robot );
}


/**
 * Transfer the soul to the given robot and rotate all
 * other robots so that they look at the player.
 *
 * @param robot : robot to which the soul will be transfered
 * @return true if transfer was successful
 */
bool cWorld::doTransferSoul( cEntityBase* robot )
{
	assert(robot);
	if( robot->getType() != eRobot )
  	return false;

	// remember old position, since soul looks at old position after transfer
	int oldI=mSoul->getIndexI();
	int oldJ=mSoul->getIndexJ();
	f32 oldGroundLevel=mSoul->getGroundLevel();

	// old robot becomes visible again, new robot gets invisible
	mSoul->getRobot()->getNode()->setVisible( true );
	robot->getNode()->setVisible( false );
	mSoul->setRobot( robot );
	
	// set the new position and where the player looks at
	mSoul->setIndex( robot->getIndexI(), robot->getIndexJ() );
	mSoul->setGroundLevel( robot->getGroundLevel() );
	mSoul->setTarget( core::vector3df(oldI+0.5, oldGroundLevel+1.0, oldJ+0.5) );
	mSoul->updateView();
	
	// rotate robots
	cEntityBase* entity;
	scene::ISceneNode* node;
	core::vector3df diffVector;
	for( int m=0; m<mSize; m++ )
		for( int n=0; n<mSize; n++ ) {
			entity = mElements[m][n];
			while( entity->getNextEntity() )
				entity = entity->getNextEntity();
			if( entity->getType() != eRobot )
				continue;
			node=entity->getNode();
			diffVector=mSoul->getPosition()-node->getPosition();
			node->setRotation( core::vector3df(0.0, 180.0-atan2( diffVector.Z, diffVector.X )*180.0/core::PI, 0.0) );
		}
		
	return true;
}


bool cWorld::hyperjump()
{
	int i, j;
	bool transferred=false;
	
	// check if player stands on pedestial and is about to win
	if(mSoul->getRobot()->getPreviousEntity()->getType()==ePedestal)
		return true;

	int level=mSoul->getGroundLevel()/sqrt(2);
	
	while( !transferred ) {			    
		i=(int)(getRandomNumber()*mSize);
		j=(int)(getRandomNumber()*mSize);
		
		if( (mElements[i][j]->getLevel()<=level) && (mElements[i][j]->getNextEntity()==NULL) ) {
			mElements[i][j]->createEntity( eRobot );
			mSoul->substractFromEnergy( eRobotNrg );
			
			doTransferSoul( mElements[i][j]->getNextEntity() );
      
			printf( "hyperjump to: %i %i %i\n", i, j, mElements[i][j]->getLevel() );
			transferred=true;
		}
	}
	
	// player was transferred from world
	return false;
}


void cWorld::showStartScreen()
{
	mCamera->setPosition(core::vector3df(16,12,-15));
	mCamera->setTarget(core::vector3df(16,0,16));
	core::vector3df rotation = mCamera->getRotation();
	
	printf("Camera rotation %f\n", rotation.Y);
}


int cWorld::getLandscapeCode(unsigned int level)
{
  mt19937* rng = new mt19937( ((unsigned long)(level)+8749UL)*23095UL );
  unsigned int mCode=(unsigned int)(rng->genrand_real1()*100000000.0);
  
	// free RNG
	delete rng;

	return mCode;
}


/*!
 * Game Logic
 *
 * @param time : game time
 */
void cWorld::processEvents(u32 time)
{
	// check every entity if there is something todo
	cEntityBase* entity, *tempEntity;
	for( int m=0; m<mSize; m++ )
		for( int n=0; n<mSize; n++ ) {
			entity = mElements[m][n];
			
			do {
				if(entity->processEvents(time)<0) {
					tempEntity = entity;
					entity = entity->getPreviousEntity();
					entity->setNextEntity(NULL);  // no next entity possible
					delete tempEntity;
					checkDefeated();
				}
			} while((entity=entity->getNextEntity()));
		}

	// process other events
	mEventManager->process(time);
}


cEntityBase* cWorld::checkForSources(bool** view)
{
	cEntityBase* entity;

	// first test if soul is in field of cerberus' view
	// the soul is always the first target
	if(view[mSoul->getIndexI()][mSoul->getIndexJ()]) {
		mSoul->setSeenFlag(true);
		entity = mElements[mSoul->getIndexI()][mSoul->getIndexJ()];
		do 
			entity = entity->getNextEntity();
		while(entity->getNextEntity());
		
		assert(entity->getType()==eRobot);
		return entity;
	} else
		mSoul->setSeenFlag(false);
	
	// then look if there is any other entity in the field of view
	// which has more energy than a tree
	for(int m=0; m<mSize; m++)
		for(int n=0; n<mSize; n++) {
			if(view[m][n]) {
				entity = mElements[m][n]->getNextEntity();
				if(entity) {
					if(entity->getType()==eBoulder || entity->getType()==eRobot) {
						while(entity->getNextEntity())
							entity = entity->getNextEntity();
						return entity;
					}
				}
			}
		}
			
	// didn't find anything
	return NULL;
}


void cWorld::checkDefeated()
{
	cEntityBase* entity;
	
	// check if there is any cerberus left
	for(int m=0; m<mSize; m++)
		for(int n=0; n<mSize; n++) {
			entity = mElements[m][n];
			while(entity->getNextEntity())
				entity = entity->getNextEntity();

			eEntity type = entity->getType();
			if(type==eSentry || type==eSentinel)
				return;
		}
	
	// no sentinel/sentry found -> world is defeated
	printf("World is defeated\n");
	mDefeated = true;
}
