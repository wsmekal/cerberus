

#include "console.h"

IC_Console* mLuaConsole;

// lua hooks and function replacements for embedding
static int luaPrint( lua_State* L )
{
	if( mLuaConsole ) {
		stringc message="";
	
		for( int i=1; i<=lua_gettop(L); i++ ) {
			if( i>1 )
				message.append( '\t' );
			if( lua_isstring(L, i) )
				message.append( lua_tostring(L, i) );
			else if( lua_isnil(L, i) )
				message.append( "nil" );
			else if( lua_isboolean(L, i) )
				message.append( lua_toboolean(L, i) ? "true" : "false" );
			else {
				message.append( lua_typename(L, lua_type(L, i)) );
				message.append( ':' );
				message.append( stringc((unsigned int)(lua_topointer(L, i))) );
			}
		}
		//message.append( '\n' );
		
		mLuaConsole->appendMessage( IC_StrConv::toWideString(message) );
  }
  
  return 0;  // no items put onto stack
}


static int luaAlert( lua_State *L )
{
    if( mLuaConsole )
			mLuaConsole->appendMessage( IC_StrConv::toWideString(stringc(lua_tostring(L, 1))) );

    return 0;
}


//! Log a unicode message to the sink.
void IC_MessageSink::logMessage( irr::ELOG_LEVEL logLevel, const stringw message )
{
	stringw wstr = getLevelTag( logLevel );
	wstr += message;
	appendMessage( wstr );
}


//! get a string for a log level
const stringw IC_MessageSink::getLevelTag( irr::ELOG_LEVEL logLevel )
{
	switch( logLevel ) {
	case irr::ELL_INFORMATION:
		return L"<info> ";
	case irr::ELL_WARNING:
		return L"<warning> ";
	case irr::ELL_ERROR:
		return L"<error> ";
	default:
		return L"";
	}
}


//! the tilde (~/`) key 
const wchar_t IC_Console::IC_KEY_TILDE = 0xc0;


//! constructor
IC_Console::IC_Console( lua_State *L ) : mLuaState( L ), bVisible( false ), guiFont( 0 ),
                                         consoleRect( 0, 0, 0, 0 ), consoleHistoryIndex( 0 ) 
{
  mLuaConsole = this;

  // hook print and alert/error function into lua state, so that
  // output is now redirected to irrLuaConsole
  lua_register( mLuaState, "print", luaPrint );	
  lua_register( mLuaState, "_ALERT", luaAlert );
}


//! (re)initialize the console with current config
void IC_Console::initializeConsole( irr::IrrlichtDevice* device )
{
  irr::gui::IGUIEnvironment* guienv=device->getGUIEnvironment();
  
	//load the font
	guiFont = guienv->getFont( consoleConfig.fontName.c_str() );
	if( guiFont == 0 )
		guiFont = guienv->getBuiltInFont();
		
	// TODO: log message, not cout
	cout << "font loaded!!" << endl;

	//calculate the console rectangle
	calculateConsoleRect( device->getVideoDriver()->getScreenSize() );

	//append a message
	appendMessage( L"IrrLuaConsole initialized" );

	//resize message array
	resizeMessages();
}


//! loads a few default commands into the console
void IC_Console::loadDefaultCommands( irr::IrrlichtDevice* device )
{
}


//! resize the message count
void IC_Console::resizeMessages()
{
	u32 maxLines = 0;
	u32 lineHeight = 0;
	s32 fontHeight = 0;
	if(calculateLimits(maxLines,lineHeight,fontHeight))
	{
		u32 messageCount = consoleMessages.size();
		if(messageCount > maxLines)
		{
			consoleMessages.erase(0,messageCount - maxLines);
		}
	}
}


//! should console be visible
bool IC_Console::isVisible()
{
	return bVisible;
}


//! set the visibility of the console
void IC_Console::setVisible( bool bV )
{
	bVisible = bV;
}


//! toggle the visibility of the console
void IC_Console::toggleVisible()
{
	setVisible( !isVisible() );
}


//! add a unicode message to the sink
void IC_Console::appendMessage( const stringw message )
{
	consoleMessages.push_back( message );
	resizeMessages();
}


//! clear all the messages in the sink
void IC_Console::clearMessages()
{
	consoleMessages.clear();
}


//! render the console (it internally checks if the console is visible)
void IC_Console::renderConsole(irr::gui::IGUIEnvironment* guienv, irr::video::IVideoDriver *videoDriver, const u32 deltaMillis)
{
	// render only if the console is visible
	if(isVisible())
	{
		//if bg is to be drawn fill the console bg with color
		if(consoleConfig.bShowBG)
		{
			//draw the bg as per configured color
			videoDriver->draw2DRectangle(consoleConfig.bgColor,consoleRect);
		}
		//we calculate where the message log shall be printed and where the prompt shall be printed
		rect<s32> textRect,shellRect;
		calculatePrintRects(textRect,shellRect);


		//now, render the messages
		u32 maxLines, lineHeight;
		s32 fontHeight;
		if(!calculateLimits(maxLines,lineHeight,fontHeight))
		{
			return;
		}
		//calculate the line rectangle
		rect<s32> lineRect(textRect.UpperLeftCorner.X,textRect.UpperLeftCorner.Y,textRect.LowerRightCorner.X,textRect.UpperLeftCorner.Y + lineHeight);
		for(u32 i = 0; i < consoleMessages.size(); i++)
		{
			//we draw each line with the configured font and color vertically centered in the rectangle
			guiFont->draw(consoleMessages[i].c_str(), lineRect, consoleConfig.fontColor, false, true);

			//update line rectangle
			lineRect.UpperLeftCorner.Y += lineHeight;
			lineRect.LowerRightCorner.Y += lineHeight;
		}

		
		//now, render the prompt
		stringw shellText = IC_StrConv::toWideString(consoleConfig.prompt);
		shellText += L"$>";
		shellText += currentCommand;

		//for blinking cursor
		static u32 blinkCounter = 0;
		blinkCounter ++;
		if(blinkCounter > 100)
		{
			blinkCounter = 0;
		}
		else if(blinkCounter <= 50)
		{
			shellText += L"_";
		}
		
		//draw the prompt string
		guiFont->draw(shellText.c_str(),shellRect,consoleConfig.fontColor,false,true);
		
	}
}


//! handles a key press when console is active/visible
void IC_Console::handleKeyPress( wchar_t keyChar, irr::EKEY_CODE keyCode, bool bShiftDown, bool bControlDown )
{
	// first we process keyChar, since it's 0 if a functional key is pressed, e.g. return
	// since e.g. the up key has the same keyCode as "(", this character would never be printed
	if( keyChar ) {
		// process other keys
		wchar_t buf[2];
		buf[0] = keyChar;
		buf[1] = 0;
		stringw astr = buf;
		if( bShiftDown )
			astr.make_upper();
		currentCommand += astr;
	} else if( keyCode == irr::KEY_RETURN ) {
		// handle return key
		addToHistory( currentCommand );
		handleCommandstringc( currentCommand );
		currentCommand = L"";
		consoleHistoryIndex = 0;
	}	else if( keyCode == irr::KEY_BACK || keyCode == irr::KEY_DELETE || (keyCode == irr::KEY_KEY_H && bControlDown) ) {
		// handle backspace, delete, ctrl+h
		if( currentCommand.size() > 0 )
			currentCommand = currentCommand.subString( 0, currentCommand.size()-1 );
	}	else if( keyCode == irr::KEY_UP ) {
		// handle key up
		if( consoleHistory.size() > 0 ) {
			s32 index = consoleHistory.size() - 1 - consoleHistoryIndex;
			if( index >= 0 && index < (s32)(consoleHistory.size()) ) {
				consoleHistoryIndex++;
				currentCommand = consoleHistory[index];
			} else
				consoleHistoryIndex = 0;
		}	else
			consoleHistoryIndex = 0;
	}	else if( keyCode == irr::KEY_DOWN ) {
		// handle key down
		if( consoleHistory.size() > 0 ) {
			s32 index = consoleHistory.size() - consoleHistoryIndex;
			if( index >= 0 && index < (s32)(consoleHistory.size()) ) {
				consoleHistoryIndex--;
				currentCommand = consoleHistory[index];
			}	else
				consoleHistoryIndex = consoleHistory.size() - 1;
		}	else
			consoleHistoryIndex = 0;
	}	else if( keyCode == irr::KEY_TAB ) {
		// handle tab key
		tabComplete();
	}
}


//! handle the current command string
void IC_Console::handleCommandstringc( stringw& wstr )
{
	//check if it is a command
	if( wstr.size()>0 && wstr[0]==(wchar_t)'\\' ) {
		stringw cmdLine = wstr.subString( 1, wstr.size()-1 );
		cout << "Command Received : " << cmdLine << endl;

		//append the message
		stringw msg = L">> Command : ";
		msg += cmdLine;
		appendMessage( msg );

		// call the lua interpreter
    /*int ret_val =*/ lua_dostring( mLuaState, IC_StrConv::toString(cmdLine.c_str()).c_str() );
	} else {
		//add to console
		appendMessage( wstr );
	}
}


//! add to history and readjust history
void IC_Console::addToHistory(stringw& wstr)
{
	if(consoleHistory.size() >= consoleConfig.commandHistorySize)
		consoleHistory.erase(0,1);
	consoleHistory.push_back(wstr);
}


//! calculate the whole console rect
void IC_Console::calculateConsoleRect(const irr::core::dimension2d<u32>& screenSize)
{
	if(consoleConfig.dimensionRatios.X == 0 || consoleConfig.dimensionRatios.Y == 0)
	{
		consoleRect = rect<s32>(0,0,0,0);
	}
	else
	{
		//calculate console dimension
		dimension2d<s32> consoleDim = dimension2d<s32>(screenSize);
		consoleDim.Width = (s32)((f32)consoleDim.Width  * consoleConfig.dimensionRatios.X);
		consoleDim.Height= (s32)((f32)consoleDim.Height * consoleConfig.dimensionRatios.Y);

		//set vertical alignment
		if(consoleConfig.valign == VAL_TOP)
		{
			consoleRect.UpperLeftCorner.Y = 0;
		}
		else if(consoleConfig.valign == VAL_BOTTOM)
		{
			consoleRect.UpperLeftCorner.Y = screenSize.Height - consoleDim.Height;
		}
		else if(consoleConfig.valign == VAL_MIDDLE)
		{
			consoleRect.UpperLeftCorner.Y = (screenSize.Height - consoleDim.Height) / 2; 
		}

		//set horizontal alignment
		if(consoleConfig.halign == HAL_LEFT)
		{
			consoleRect.UpperLeftCorner.X = 0;
		}
		else if(consoleConfig.halign == HAL_RIGHT)
		{
			consoleRect.UpperLeftCorner.X = screenSize.Width - consoleDim.Width;
		}
		else if(consoleConfig.halign == HAL_CENTER)
		{
			consoleRect.UpperLeftCorner.X = (screenSize.Width - consoleDim.Width) / 2; 
		}
		
		//set the lower right corner stuff
		consoleRect.LowerRightCorner.X = consoleRect.UpperLeftCorner.X + consoleDim.Width;
		consoleRect.LowerRightCorner.Y = consoleRect.UpperLeftCorner.Y + consoleDim.Height;
	}
}

//! calculate the messages rect and prompt / shell rect
void IC_Console::calculatePrintRects(rect<s32>& textRect,rect<s32>& shellRect)
{
	u32 maxLines, lineHeight;
	s32 fontHeight;
	if(calculateLimits(maxLines,lineHeight,fontHeight))
	{
		
		//the shell rect
		shellRect.LowerRightCorner.X = consoleRect.LowerRightCorner.X;
		shellRect.LowerRightCorner.Y = consoleRect.LowerRightCorner.Y;
		shellRect.UpperLeftCorner.X = consoleRect.UpperLeftCorner.X;
		shellRect.UpperLeftCorner.Y = consoleRect.LowerRightCorner.Y - lineHeight;

		//calculate text rect
		textRect.UpperLeftCorner.X = consoleRect.UpperLeftCorner.X;
		textRect.UpperLeftCorner.Y = consoleRect.UpperLeftCorner.Y;
		shellRect.LowerRightCorner.X = consoleRect.LowerRightCorner.X;
		shellRect.LowerRightCorner.Y = shellRect.UpperLeftCorner.Y;


	}
	else
	{
		textRect = rect<s32>(0,0,0,0);
		shellRect = rect<s32>(0,0,0,0);
	}
}


bool IC_Console::calculateLimits(u32& maxLines, u32& lineHeight,s32& fontHeight)
{
	u32 consoleHeight = consoleRect.getHeight();
	if(guiFont != 0 && consoleHeight > 0)
	{
		fontHeight = guiFont->getDimension(L"X").Height;
		lineHeight = fontHeight + consoleConfig.lineSpacing;
		maxLines = consoleHeight / lineHeight;
		if(maxLines > 2)
		{
			maxLines -= 2;
		}
		return true;
	}
	else
	{
		return false;
	}
}


void IC_Console::tabComplete()
{
	if(currentCommand.size() == 0)
	{
		return;
	}
	else if(currentCommand[0] != (wchar_t)('\\'))
	{
		return;
	}
	stringw ccStr = currentCommand.subString(1,currentCommand.size() - 1);
	array<stringw> names;
	//getRegisteredCommands(names);
	for(u32 i = 0; i < names.size(); i++)
	{
		stringw thisCmd = names[i];
		if(thisCmd.size() == ccStr.size())
		{
			if(thisCmd == ccStr)
			{
				return;
			}
		}
		else if(thisCmd.size() > ccStr.size())
		{
			if(thisCmd.subString(0,ccStr.size()) == ccStr)
			{
				currentCommand = L"\\";
				currentCommand += thisCmd;

				return;
			}
		}
	}
}


stringw IC_StrConv::toWideString( const stringc str )
{
	int len = str.size() + 1;
	wchar_t* buf = new wchar_t[len];
	
	::mbstowcs(buf,str.c_str(),len);
	stringw wstr = buf;
	delete[] buf;
	return wstr;
}


stringc IC_StrConv::toString( const stringw str )
{
	int len = str.size() + 1;
	c8* buf = new c8[len];
	::wcstombs( buf, str.c_str(), len );
	stringc wstr = buf;
	delete[] buf;
	return wstr;
}


