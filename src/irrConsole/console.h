
#ifndef __IRRCONSOLE_CONSOLE_H__
#define __IRRCONSOLE_CONSOLE_H__

// stl headers
#include <iostream>
#include <map>

#include <wchar.h>

// irrlicht headers
#include <irrlicht.h>
using irr::core::array;
using irr::core::vector3df;
using irr::core::vector2df;
using irr::core::rect;
using irr::core::dimension2d;
using irr::core::stringw;
using irr::core::stringc;
using namespace irr;

using std::cout;
using std::cerr;
using std::endl;
using std::ostream;
using std::map;

// lua headers
extern "C" {
	#include <lua.h>
	#include <lualib.h>
	#include <lauxlib.h>
}	


//! a string conversion class for converting between UTF-8 and UTF-16 strings
class IC_StrConv
{
public:
	static stringw toWideString( const stringc str );
	static stringc toString( const stringw str );
};


//! overloaded operator for inserting an ansi string to the stl ostream
inline ostream& operator<<( ostream& os, const stringc& s )
{
	return ( os << s.c_str() );
}


//! overloaded operator for inserting a wide string to the stl ostream
inline ostream& operator<<( ostream& os, const stringw& ws )
{
	return ( os << IC_StrConv::toString(ws) );
}


//! the message sink interface
class IC_MessageSink
{
public:
	//! constructor
	IC_MessageSink() {};

	//! destructor
	virtual ~IC_MessageSink() {};
	
	// log a ansi/unicode message to the sink
	void logMessage( irr::ELOG_LEVEL logLevel, const stringw message );
	
	void logMessage( irr::ELOG_LEVEL logLevel, const stringc message )
		{ logMessage( logLevel, IC_StrConv::toWideString(message) ); }
	 
	void logMessage( irr::ELOG_LEVEL logLevel, const irr::c8* message )
		{ logMessage( logLevel, IC_StrConv::toWideString(message) ); }
	
	//! get a string for a log level
	virtual const stringw getLevelTag( irr::ELOG_LEVEL logLevel );
    
	//! add a unicode message to the sink
	virtual void appendMessage( const stringw message ) = 0;
  
	//! clear all the messages in the sink
	virtual void clearMessages() = 0;
  
	//! set the visibility
	virtual void toggleVisible() = 0;
};


//! an alignment enumeration for vertical alignment
enum IC_VerticalAlignment
{
	VAL_TOP = 0,     //<! top	
	VAL_MIDDLE = 1,  //<! middle
	VAL_BOTTOM = 2   //<! bottom
};


//! an alignment enumeration for horizontal alignment
enum IC_HorizontalAlignment
{
	HAL_LEFT = 0,    //<! left
	HAL_CENTER = 1,  //! center
	HAL_RIGHT = 2    //! right
};


//! the console config structure
struct IC_ConsoleConfig
{
public:
	//! constructor
	IC_ConsoleConfig()
		{ setDefaults(); }
	
	//! set the defaults on the console config
	void setDefaults()
	{
		dimensionRatios.X = 1.0f;
		dimensionRatios.Y = 0.6f;
		lineSpacing = 2;
		indent = 1;
		valign= VAL_TOP;
		halign= HAL_LEFT;
		bShowBG = true;
		bgColor = irr::video::SColor(150,10,10,70);
		fontColor = irr::video::SColor(200,200,200,200);
		fontName = "data/font/console.bmp";
		prompt = "console";
		commandHistorySize = 10;
	}

	//! this contains the Width and Height ratios to the main view port (0 - 1)
	vector2df dimensionRatios;

	//! this is the spacing between two lines in pixels (Default / Min : 2)
	u32 lineSpacing;

	//! this is the indentation for each line in pixels (Default / Min : 1)
	u32 indent;

	//! this is the alignment flag for the vertical placement of the console
	IC_VerticalAlignment valign;

	//! this is the alignment flag for the horizontal alignment of the console
	IC_HorizontalAlignment halign;

	//! this is the flag indicating if the console BG should be drawn
	bool bShowBG;

	//! this is the color for the console BG
	irr::video::SColor bgColor;

	//! this is the font color for the console
	irr::video::SColor fontColor;

	//! this is the font name
	stringc fontName;

	//! this is the prompt string displayed as prompt$>
	stringc prompt;

	//! this is the command history length (defaults to 10)
	u32 commandHistorySize;
};


//! A Quake Like console class
class IC_Console : public IC_MessageSink
{
public:
	static const wchar_t IC_KEY_TILDE;

	//! constructor
	IC_Console( lua_State *L );

	//! destructor
	virtual ~IC_Console()
		{}

	//! get the console config reference
	IC_ConsoleConfig& getConfig()
    { return consoleConfig; }

	//! (re)initialize the console with current config
	void initializeConsole( irr::IrrlichtDevice* device );

	//! loads a few default commands into the console
	void loadDefaultCommands( irr::IrrlichtDevice* device);

	//! should console be visible
	bool isVisible();

	//! set the visibility of the console
	void setVisible( bool bV );
    
	//! toggle the visibility of the console
	void toggleVisible();

	//
	//	Message Sink implementation
	//
	//! add a unicode message to the sink
	void appendMessage( const stringw message );
  
	//! clear all the messages in the sink
	void clearMessages();

	//
	//	console rendering 
	//
	//! render the console (it internally checks if the console is visible)
	void renderConsole(irr::gui::IGUIEnvironment* guienv, irr::video::IVideoDriver *videoDriver, const u32 deltaMillis);

	//
	//	console message handling
	//
	//! handles a key press when console is active/visible
	void handleKeyPress(wchar_t keyChar, irr::EKEY_CODE keyCode, bool bShiftDown, bool bControlDown);

private:
	//! parses and handles the current command string
	void handleCommandstringc(stringw& wstr);
	
	//! add a line to history
	void addToHistory(stringw& line);
	
	//! calculate the whole console rect
	void calculateConsoleRect(const irr::core::dimension2d<u32>& screenSize);
	
	//! calculate the messages rect and prompt / shell rect
	void calculatePrintRects(rect<s32>& textRect,rect<s32>& shellRect);
	
	//! calculate the various limits of the console
	bool calculateLimits(u32& maxLines, u32& lineHeight,s32& fontHeight);
	
	//! resize the message count
	void resizeMessages();
	
	//! do a tab completion
	void tabComplete();
  
private:
  //! console config data
	IC_ConsoleConfig consoleConfig;
	
  //! handle of lua interpreter
  lua_State *mLuaState;

	//! visibility flag
	bool bVisible;
	
	//! the font of the console
	irr::gui::IGUIFont* guiFont;

	//! the console rectangle
	irr::core::rect<s32> consoleRect;

	//! the console messages
	array<stringw> consoleMessages;

	//! the command history
	array<stringw> consoleHistory;

	//! the history pointer / index
	u32 consoleHistoryIndex;

	//! the current command string
	stringw currentCommand;
};

#endif  // __IRRCONSOLE_CONSOLE_H__
