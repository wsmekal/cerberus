
// include stl headers
#include <string>

// include irrlicht headers
#include "irrlicht.h"
using namespace irr;

// include irrlicht headers
#if defined(USE_IRRKLANG)
	#include "irrKlang.h"
#endif

// cerberus headers
#include "cerberus.h"


/*!
 * Constructor of the audio device, which needs to know the path
 * to all the media files. Here we initialize the device using
 * either irrKlang (implemented) or SDL (not implemented).
 * @todo correct exiting if irrKlang couldn't be opened
 * @todo implement sdl
 *
 * @param mediaPath : path to media files
 */
cAudioDevice::cAudioDevice( std::string mediaPath  ) : mMusicVolume( 0.5 ), mSoundVolume( 1.0 ), mMusic( NULL )
{
	mMediaPath = mediaPath;
	
#if defined(USE_IRRKLANG)
	// open irrKlang Device
	irrKlang = irrklang::createIrrKlangDevice();

	if( !irrKlang )
		exit( -1 );

#elif defined(USE_SDL)
#endif

	preloadSounds();
}


/*!
 * Loads all sounds which are used in the game into memory.
 */
void cAudioDevice::preloadSounds()
{
#if defined(USE_IRRKLANG)
	mSoundSources[eCerberusTurns] = irrKlang->addSoundSourceFromFile(
	                       formatString("%s/sounds/cerberus_turns.wav", mMediaPath.c_str()) ); 
#elif defined(USE_SDL)
#endif
}


/*!
 * Deconstructor takes care that all is stopped correctly. 
 */
cAudioDevice::~cAudioDevice()
{
#if defined(USE_IRRKLANG)
	irrKlang->stopAllSounds();
  irrKlang->removeAllSoundSources();
	irrKlang->drop();
#elif defined(USE_SDL)
#endif
}


/*!
 * Set the position where the player is in the landscape. This will allow
 * the irrKlang library to correctly calculate the 3D sound.
 *
 * @param pos : Position of the player
 * @param lookdir : Direction in which the player looks
 * @param velPerSecond : Velocity of the player (possible Doppler effect)
 * @param upVector : Vector which goes upward from the player
 */
void cAudioDevice::setListenerPosition( const irrklang::vec3df& pos, const irrklang::vec3df& lookdir,
                                        const irrklang::vec3df& velPerSecond, const irrklang::vec3df& upVector )
{
#if defined(USE_IRRKLANG)
  irrKlang->setListenerPosition( pos, lookdir );
#elif defined(USE_SDL)
  // no 3d sound implementation in SDL
#endif
}


/*!
 * Play an actual (already preloaded) sound at a certain position.
 *
 * @param soundID : Id of the (preloaded) sound to be played
 * @see eSoundSource
 * @param pos : Position of the sound source to be played
 */
void cAudioDevice::playSoundAtPosition(eSoundSource soundID, const core::vector3df& position)
{
#if defined(USE_IRRKLANG)
	irrklang::vec3df mPosition(position.X, position.Y, position.Z);
  irrKlang->play3D(mSoundSources[soundID], mPosition);
#elif defined(USE_SDL)
  // no 3d sound implementation in SDL
#endif
}


/*!
 * Play music file from the media directory.
 *
 * @param filename : Name of the music file (relative to the media directory)
 */
void cAudioDevice::playMusic( const std::string filename )
{
#if defined(USE_IRRKLANG)
	// stop music
	if( mMusic ) {
		mMusic->stop();
		mMusic->drop();
		mMusic = NULL;
	}

	std::string mFilename = mMediaPath + "/" + filename; 
	mMusic = irrKlang->play2D( mFilename.c_str(), true, false, true );
	if( mMusic )
		mMusic->setVolume( mMusicVolume );
#elif defined(USE_SDL)
#endif
}


/*!
 * Set music volume.
 *
 * @param volume : Music volume from 0.0 to 1.0
 */
void cAudioDevice::setMusicVolume( f32 volume )
{
	mMusicVolume = volume;

#if defined(USE_IRRKLANG)
	if( mMusic )
		mMusic->setVolume( mMusicVolume );
#elif defined(USE_SDL)
#endif
}


/*!
 * Set sound volume.
 *
 * @param volume : Sound volume from 0.0 to 1.0
 */
void cAudioDevice::setSoundVolume( f32 volume )
{
	mSoundVolume = volume;

#if defined(USE_IRRKLANG)
	for( int i=0; i<eMaxSound; i++ )
		if( mSoundSources[i] )
			mSoundSources[i]->setDefaultVolume( mSoundVolume );
#elif defined(USE_SDL)
#endif
}
