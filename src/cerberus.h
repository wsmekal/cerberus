
#ifndef __CERBERUS_H
#define __CERBERUS_H

#include <string>

// lua headers
extern "C" {
	#include <lua.h>
	#include <lualib.h>
	#include <lauxlib.h>
}

// irrlicht headers
#ifdef USE_IRRKLANG
	#include "irrKlang.h"
#endif
#include <irrlicht.h>
using namespace irr;

// forward declarations
class mt19937;

class cConfiguration
{
public:
  bool loadFromFile( lua_State* L, const char* filename );   //!< load settings from lua file

public:
  bool fullscreen;     //!< true for fullscreen mode
  u32 width;           //!< width of screen/window
  u32 height;          //!< height of screen/window
  bool vsync;          //!< true for vertical synchronisation
  u32 screenDepth;     //!< bits per pixel if fullscreen mode
  size_t driverType;   //!< driver type (OpenGL, ...)

  double max_fps;
  double fps_period;
};

/*!
 * Ids for the various sound sources.
 * @see cAudioDevice::playSoundAtPosition
 */
enum eSoundSource { eCerberusTurns=0, eMaxSound };
  
class cAudioDevice
{
public:
  cAudioDevice( std::string mediaPath );
  ~cAudioDevice( void );

  // set position of listener
  void setListenerPosition( const irrklang::vec3df& pos, const irrklang::vec3df& lookdir,
                            const irrklang::vec3df& velPerSecond=irrklang::vec3df(0, 0, 0),
                            const irrklang::vec3df& upVector=irrklang::vec3df(0, 1, 0) );

  void playSoundAtPosition(eSoundSource soundID, const core::vector3df& position);
  void playMusic( const std::string filename );

  void setMusicVolume( f32 volume );
  f32 getMusicVolume()
  	{ return mMusicVolume; }  //!< Returns the music volume
  void setSoundVolume( f32 volume );
  f32 getSoundVolume()
  	{ return mSoundVolume; }  //!< Returns the sounds volume
	void preloadSounds();
	
private:
	f32 mMusicVolume;   //!< Volume of music
	f32 mSoundVolume;   //!< Volume of sounds
	std::string mMediaPath;  //!< Path to media files
#if defined(USE_IRRKLANG)
	irrklang::ISoundSource* mSoundSources[eMaxSound];  //!< Sound source if cerberus turns
	irrklang::ISoundEngine* irrKlang;	
	irrklang::ISound* mMusic;
#endif
};


/*!
 * Base class for all events. Basically a list element 
 * with a pure virtual process() method.
 */
class cEventBase
{
public:
	cEventBase() : mNextEvent( NULL ), mPreviousEvent( NULL ), mDrop( false )
	  { }  //!< Constructor of event base class
	virtual ~cEventBase()
		{ } //!< Deconstructor of event base class
	
	cEventBase* getNextEvent()
	  { return mNextEvent; }  //!< Get next event in list
	void setNextEvent( cEventBase* event )
	  { mNextEvent = event; }  //!< Set next event in list
	cEventBase* getPreviousEvent()
	  { return mPreviousEvent; }  //!< Get previous event in list
	void setPreviousEvent( cEventBase* event )
	  { mPreviousEvent = event; }  //!< Set previous event in list
	  
	void drop()
		{ mDrop = true; }  //!< "drop" the event, i.e. event will be deleted when removed from list
	bool isDropped()
		{ return mDrop; }  //!< Return drop status
	  
	virtual void process( u32 time )=0;  //!< Actually process the event
	
private:
	cEventBase* mNextEvent;  //!< Next event in list
	cEventBase* mPreviousEvent;  //!< Previous event in list
	
	bool mDrop;  //!< Drop status, true if event should be deleted when removed from list
};


/*!
 * The event manager takes care of all registered events.
 * When the process method is called all the registered
 * events in the list are processed.
 */
class cEventManager
{
public:
	cEventManager();  //!< Constructor of event manager class
	~cEventManager();  //!< Deconstructor of event manager class
	
	void registerEvent( cEventBase* event );  //!< Add a new event to the list
	void removeEvent( cEventBase* event );  //!< Remove an event from the list
	
	void process( u32 time );  //!< Process all registered events in the list
	
private:
	cEventBase* mFirstEvent;  //!< first element in the event list
	cEventBase* mLastEvent;  //!< last element in the event list
};


//! sprintf-like function which returns a pointer to the processed string
char* formatString( char* format, ... );

#endif // __CERBERUS_H
