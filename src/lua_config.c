/*
 * lua_config provides some helper functions to get variables from 
 * the global lua name space or from a given table. This should
 * help to read out a lua config file.
 *
 * Author: Werner Smekal, 2007
 */

#include "lua_config.h"

/* #include <errno.h> */
#include <string.h>
#include <stdio.h>

/* This function searches for the boolean variable 'name' in the configuration
   file and returns its value (converted to integer). */
int get_boolean( lua_State* L, const char* name, const int mDefault )
{
  int i;

  lua_getglobal( L, name );
  if( !lua_isboolean(L, -1) )
    i=mDefault;
  else
    i=(int)lua_toboolean( L, -1 );
  lua_pop( L, 1 );
  
  return i;
}


/* This function searches for the float variable 'name' in the configuration
   file and returns its value. */
double get_number( lua_State* L, const char* name, const double mDefault )
{
  double f;

  lua_getglobal( L, name );
  if( !lua_isnumber(L, -1) )
    f=mDefault;
  else
    f=(double)lua_tonumber( L, -1 );
  lua_pop( L,1 );
  
  return f;
}


/* This function searches for the string variable 'name' in the configuration
   file and copies the string into buffer. */
void get_string( lua_State* L, const char* name, char* buffer, size_t bufferSize, const char* mDefault )
{
  lua_getglobal( L, name );
  if( !lua_isstring(L, -1) )
    snprintf( buffer, bufferSize, "%s", mDefault );
  else
    snprintf( buffer, bufferSize, "%s", lua_tostring(L, -1) );
  lua_pop( L, 1 );
}


/* This function searches for the float variable 'table.name' in the configuration
   file and returns its value. */
double get_number_from_table( lua_State* L, const char* table, const char* name, const double mDefault )
{
  double f;

	lua_getglobal( L, table );
	if( !lua_istable(L, -1) )
    f=mDefault;
  else {
    lua_pushstring( L, name );
    lua_gettable( L, -2 );
    if( !lua_isnumber(L, -1) )
      f=mDefault;
    else
      f=(double)lua_tonumber( L, -1 );

    lua_pop( L, 1 );    /* remove string */
  }
  lua_pop( L, 1 );    /* remove table */

  return f;
}


/* This function searches for the boolean variable 'table.name' in the configuration
   file and returns its value (converted to integer). */
int get_boolean_from_table( lua_State* L, const char* table, const char* name, const int mDefault )
{
  int i;

	lua_getglobal( L, table );
	if( !lua_istable(L, -1) ) 
    i=mDefault;
  else {
    lua_pushstring( L, name );
    lua_gettable( L, -2 );
    if( !lua_isboolean(L, -1) )
      i=mDefault;
    else
      i=(int)lua_toboolean( L, -1 );

    lua_pop( L, 1 );   /* remove string */
  }
  lua_pop( L, 1 );   /* remove table */

  return i;
}


/* This function searches for the string variable 'table.name' in the configuration
   file and copies the string into buffer. */
void get_string_from_table( lua_State* L, const char* table, const char* name, char* buffer, const size_t bufferSize, const char* mDefault )
{
	lua_getglobal( L, table );
	if( !lua_istable(L, -1) )
    snprintf( buffer, bufferSize, "%s", mDefault );
  else {
    lua_pushstring( L, name );
    lua_gettable( L, -2 );
    if( !lua_isstring(L, -1) )
      snprintf( buffer, bufferSize, "%s", mDefault );
    else
      snprintf( buffer, bufferSize, "%s", lua_tostring(L, -1) );
    lua_pop( L, 1 );    /* remove string */
  }
  lua_pop( L, 1 );    /* remove table */
}


/* interface for a lua function call from the lua book.
   example: call_lua_function( L,   "function", "dd>d", x, y, &z )
   where "dd>d" means two double input, one double output. 
   d ... double, i ... integer, s ... string */
void call_lua_function( lua_State* L, const char *func, const char *sig, ... )
{
  va_list vl;
  int narg, nres;  /* number of arguments and results */

  va_start(vl, sig);
  lua_getglobal( L, func );  /* get function */

  /* push arguments */
  narg = 0;
  while (*sig) {  /* push arguments */
    switch (*sig++) {

      case 'd':  /* double argument */
        lua_pushnumber(L, va_arg(vl, double));
        break;

      case 'i':  /* int argument */
        lua_pushnumber(L, va_arg(vl, int));
        break;

      case 's':  /* string argument */
        lua_pushstring(L, va_arg(vl, char *));
        break;

      case '>':
        goto endwhile;

      default:
        printf( "invalid option (%c)", *(sig - 1) );
    }
    narg++;
    luaL_checkstack(L, 1, "too many arguments");
  } endwhile:

  /* do the call */
  nres = strlen(sig);  /* number of expected results */
  if (lua_pcall(L, narg, nres, 0) != 0)  /* do the call */
    printf( "error running function `%s': %s\n",
             func, lua_tostring(L, -1) );

  /* retrieve results */
  nres = -nres;  /* stack index of first result */
  while (*sig) {  /* get results */
    switch (*sig++) {

      case 'd':  /* double result */
        if (!lua_isnumber(L, nres))
          printf( "wrong result type" );
        *va_arg(vl, double *) = lua_tonumber(L, nres);
        break;

      case 'i':  /* int result */
        if (!lua_isnumber(L, nres))
          printf( "wrong result type" );
        *va_arg(vl, int *) = (int)lua_tonumber(L, nres);
        break;

      case 's':  /* string result */
        if (!lua_isstring(L, nres))
          printf( "wrong result type" );
        *va_arg(vl, const char **) = lua_tostring(L, nres);
        break;

      default:
        printf("invalid option (%c)", *(sig - 1));
    }
    nres++;
  }
  va_end(vl);
}
