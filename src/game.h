

#ifndef __GAME_H
#define __GAME_H

// include irrlicht headers
#include "irrlicht.h"
using namespace irr;

// cerberus headers
#include "landscape.h"

// forward declarations
class mt19937;
class cConfiguration;
class IC_console;

enum eGameStatus { eGameStart=1, eGameRuns, eGameQuit, ePlayerIsDead, ePlayerHasWon };

class cGame : public IEventReceiver
{
public:
  // Constructor
	cGame( IrrlichtDevice* device, cConfiguration* configuration, cAudioDevice* audioDevice,
         IC_Console* console, unsigned int level );
	~cGame( void );

  // run game
  void run( void );

private:
  // Event handler
	bool OnEvent( const SEvent& event );

  // Game Interface
  void drawInterface( void );

  // Other functions
  bool isPlayerDead( void )
    { return mWorld->getSoul()->getEnergy()<=0; }

	bool isWorldDefeated( void )
	{ return mWorld->isDefeated(); }

private:
	// several pointers to irrlicht objects
	IrrlichtDevice* mDevice;
  video::IVideoDriver* mDriver;
	scene::ISceneManager* mSmgr;
	gui::IGUIEnvironment* mEnv;
  ILogger *mLogger;

  // pointer to configuration object
  cConfiguration* mConfiguration;
  
  // pointer to Audio device
  cAudioDevice* mAudioDevice;
  
  // Console
  IC_Console *mConsole;

  // pointer to landscape
  cWorld* mWorld;
	unsigned int mLevel;

  // status of the game
  eGameStatus mGameStatus;

	bool mPlayerHasWon;
	
  // pointers to textures
  video::ITexture* tIrrlichtLogo;    //!< Irrlich Logo
  video::ITexture* tCrosshair;       //!< Crosshair icon
  video::ITexture* tNrgTree;         //!< Tree energy icon
  video::ITexture* tNrgBoulder;      //!< Boulder energy icon
  video::ITexture* tNrgRobot;        //!< Robot energy icon
  video::ITexture* tNrgRobot5;       //!< Golden robot energy icon
  
  // random number generator to be commonly used
  // initialized once
  mt19937* mRNG;          //!< pointer to RNG
};


#endif // __GAME_H
